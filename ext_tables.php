<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('iwansonsstuff', 'Configuration/TypoScript', 'iwansonsStuff');

        
        if (TYPO3_MODE === 'BE') {

            $GLOBALS['TBE_STYLES']['skins']['backend']['stylesheetDirectories']['iwansonsstuff'] = 'EXT:'
           . 'iwansonsstuff'
           . '/Resources/Public/css/Backend/';

        }
    }
    
);
