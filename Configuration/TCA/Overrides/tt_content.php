<?php

$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][0][0] = 'keiner';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][1][0] = '1 Einheit';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][2][0] = '2 Einheiten';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][3][0] = '3 Einheiten';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][4][0] = '4 Einheiten';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][5][0] = '5 Einheiten';

$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][0][0] = 'keiner';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][1][0] = '1 Einheit';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][2][0] = '2 Einheiten';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][3][0] = '3 Einheiten';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][4][0] = '4 Einheiten';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][5][0] = '5 Einheiten';

$GLOBALS['TCA']['tt_content']['columns']['header_layout']['config']['items'][0][0] = 'NORMAL: Fett im Haupt-Menu & im mittleren Menu auf der Seite';

$GLOBALS['TCA']['tt_content']['columns']['header_layout']['config']['items'][6][0] = 'NUR im Haupt-Menu (nicht im mittleren Menu auf der Seite)';

$GLOBALS['TCA']['tt_content']['columns']['layout']['config']['items'][1][0] = 'Am unteren Rand anordnen';
$GLOBALS['TCA']['tt_content']['columns']['layout']['config']['items'][2][0] = '2 - nicht benutzt';
$GLOBALS['TCA']['tt_content']['columns']['layout']['config']['items'][3][0] = '3 - nicht benutzt';



// Slider FOR Startpage Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:startpageslider.title',
            'startpageslider',
            'content-image'
        ],
        'textmedia',
        'after'
);
//$GLOBALS['TCA']['tt_content']['types']['startpageslider']['columnsOverrides']['bodytext']['config'] = [
//    'enableRichtext' => 1,
//    'richtextConfiguration' => 'default'
//];

$GLOBALS['TCA']['tt_content']['types']['startpageslider'] = [
    'showitem' => '
    --palette--;;general,bodytext;Allgemeiner Störer (Ticker über alle Bilder),header_link;Weiter-Link für allgemeinen Störer,   
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:startpageslider.Images,    
    '
];
// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_startpageslider',
            'startpageslider',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_startpageslider.gif'
        ),
        'CType',
        'iwansonsstuff'
);


// iwanson tile image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagetiles.title',
            'iwansonimagetiles',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimagetiles'] = [
    'showitem' => '
    --palette--;;general,bodytext,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames11
    '
];
$GLOBALS['TCA']['tt_content']['types']['iwansonimagetiles']['columnsOverrides']['bodytext']['config'] = [
    'enableRichtext' => 1,
    'richtextConfiguration' => 'default'
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimagetiles',
            'iwansonimagetiles',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimagetiles.gif'
        ),
        'CType',
        'iwansonsstuff'
);


// admin image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimageadmin.title',
            'iwansonimageadmin',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimageadmin'] = [
    'showitem' => '
    --palette--;;general,header,header_layout,bodytext,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

$GLOBALS['TCA']['tt_content']['types']['iwansonimageadmin']['columnsOverrides']['bodytext']['config'] = [
    'enableRichtext' => 1,
    'richtextConfiguration' => 'default'
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimageadmin',
            'iwansonimageadmin',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimageadmin.gif'
        ),
        'CType',
        'iwansonsstuff'
);


// distance (mobile) Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:mobiledistance.title',
            'mobiledistance',
            'content-header'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['mobiledistance'] = [
    'showitem' => '
    --palette--;;general  
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_mobiledistance',
            'mobiledistance',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_mobiledistance.gif'
        ),
        'CType',
        'iwansonsstuff'
);


// instructor image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimageinstructor.title',
            'iwansonimageinstructor',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimageinstructor'] = [
    'showitem' => '
    --palette--;;general,header,header_layout,bodytext, 
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

$GLOBALS['TCA']['tt_content']['types']['iwansonimageinstructor']['columnsOverrides']['bodytext']['config'] = [
    'enableRichtext' => 1,
    'richtextConfiguration' => 'default'
];

// guestinstructor image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimageguestinstructor.title',
            'iwansonimageguestinstructor',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimageguestinstructor'] = [
    'showitem' => '
    --palette--;;general,header,header_layout,bodytext, 
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

$GLOBALS['TCA']['tt_content']['types']['iwansonimageguestinstructor']['columnsOverrides']['bodytext']['config'] = [
    'enableRichtext' => 1,
    'richtextConfiguration' => 'default'
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimageinstructor',
            'iwansonimageinstructor',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimageinstructor.gif'
        ),
        'CType',
        'iwansonsstuff'
);

// nimble image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagenimble.title',
            'iwansonimagenimble',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimagenimble'] = [
    'showitem' => '
    --palette--;;general,header,
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimagenimble',
            'iwansonimagenimble',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimagenimble.gif'
        ),
        'CType',
        'iwansonsstuff'
);

// brochure image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagebrochure.title',
            'iwansonimagebrochure',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimagebrochure'] = [
    'showitem' => '
    --palette--;;general,header,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimagebrochure',
            'iwansonimagebrochure',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimagebrochure.gif'
        ),
        'CType',
        'iwansonsstuff'
);



// fixed button Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonfixedbutton.title',
            'iwansonfixedbutton',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonfixedbutton'] = [
    'showitem' => '
    --palette--;;general,header,header_link,    
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonfixedbutton',
            'iwansonfixedbutton',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonfixedbutton.gif'
        ),
        'CType',
        'iwansonsstuff'
);




// very big image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimageverybig.title',
            'iwansonimageverybig',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimageverybig'] = [
    'showitem' => '
    --palette--;;general,bodytext,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];
$GLOBALS['TCA']['tt_content']['types']['iwansonimageverybig']['columnsOverrides']['bodytext']['config'] = [
    'enableRichtext' => 1,
    'richtextConfiguration' => 'default'
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimageverybig',
            'iwansonimageverybig',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimageverybig.gif'
        ),
        'CType',
        'iwansonsstuff'
);


// big image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagebig.title',
            'iwansonimagebig',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimagebig'] = [
    'showitem' => '
    --palette--;;general,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimagebig',
            'iwansonimagebig',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimagebig.gif'
        ),
        'CType',
        'iwansonsstuff'
);


// medium image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagemedium.title',
            'iwansonimagemedium',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimagemedium'] = [
    'showitem' => '
    --palette--;;general,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimagemedium',
            'iwansonimagemedium',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimagemedium.gif'
        ),
        'CType',
        'iwansonsstuff'
);


//small image Content Element -----------------------------------------------------------------------
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagesmall.title',
            'iwansonimagesmall',
            'content-image'
        ],
        'textmedia',
        'after'
);

$GLOBALS['TCA']['tt_content']['types']['iwansonimagesmall'] = [
    'showitem' => '
    --palette--;;general,    
    assets;LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimages.Images,    
    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames
    '
];

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'LLL:EXT:iwansonsstuff/Resources/Private/Language/Tca.xlf:iwansonsstuff_iwansonimagesmall',
            'iwansonimagesmall',
            'EXT:iwansonsstuff/Resources/Public/Icons/ContentElements/iwansonsstuff_iwansonimagesmall.gif'
        ),
        'CType',
        'iwansonsstuff'
);





// Crop variants startpage slider -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['startpageslider']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Default (breit)',
                'allowedAspectRatios' => [
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ]
            ],
            'portrait' => [
                'title' => 'Hochkant',
                'allowedAspectRatios' => [
                    '5:12' => [
                        'title' => 'Hoch-Format (5:12)',
                        'value' => 5/12
                    ],
                ],
            ]
        ]
    ]
];


// Crop variants for image tiles -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimagetiles']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'landscape' => [
                'title' => 'Quer',
                'allowedAspectRatios' => [
                    '23:17' => [
                        'title' => 'Hoch-Format (23:17)',
                        'value' => 23/17
                    ],
                ],
            ],
            'portrait' => [
                'title' => 'Hochkant',
                'allowedAspectRatios' => [
                    '17:23' => [
                        'title' => 'Hoch-Format (17:23)',
                        'value' => 17/23
                    ],
                ],
            ]
        ]
    ]
];




// Crop variants instructor images -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimageinstructor']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '8:29' => [
                        'title' => 'Dozenten-Hoch-Format (8:29)',
                        'value' => 8/29
                    ],
                    '23:9' => [
                        'title' => 'Dozenten-Quer-Format (23:9)',
                        'value' => 23/9
                    ],                    
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '1:4' => [
                        'title' => 'Dozenten-Hoch-Format (1:4)',
                        'value' => 1/4
                    ],
                    '13:5' => [
                        'title' => 'Dozenten-Quer-Format (13:5)',
                        'value' => 13/5
                    ],                                        
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '2:7' => [
                        'title' => 'Dozenten-Hoch-Format (2:7)',
                        'value' => 2/7
                    ],
                    '2:1' => [
                        'title' => 'Dozenten-Quer-Format (2:1)',
                        'value' => 2/1
                    ],                    
                ],
            ],
        ]
    ]
];

// Crop variants guestinstructor images -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimageguestinstructor']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '23:9' => [
                        'title' => 'Gastdozenten-Quer-Format (23:9)',
                        'value' => 23/9
                    ],                    
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '13:5' => [
                        'title' => 'Gastdozenten-Quer-Format (13:5)',
                        'value' => 13/5
                    ],                                        
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '2:1' => [
                        'title' => 'Gastdozenten-Quer-Format (2:1)',
                        'value' => 2/1
                    ],                    
                ],
            ],
        ]
    ]
];


// Crop variants nimble images -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimagenimble']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '8:29' => [
                        'title' => 'Nimble-Format (8:29)',
                        'value' => 8 / 29
                    ],
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '1:4' => [
                        'title' => 'Nimble-Format (1:4)',
                        'value' => 1 / 4
                    ],
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '2:7' => [
                        'title' => 'Nimble-Format (2:7)',
                        'value' => 2 / 7
                    ],
                ],
            ],
        ]
    ]
];

// Crop variants admin images (same as nimble!) -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimageadmin']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = $GLOBALS['TCA']['tt_content']['types']['iwansonimagenimble']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'];

// Crop variants brochure images -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimagebrochure']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '3:11' => [
                        'title' => 'Broschüren-Bild (3:11)',
                        'value' => 3 / 11
                    ],
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '8:29' => [
                        'title' => 'Broschüren-Bild (8:29)',
                        'value' => 8 / 29
                    ],
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '3:11' => [
                        'title' => 'Broschüren-Bild (3:11)',
                        'value' => 3 / 11
                    ],
                ],
            ],
        ]
    ]
];


// Crop variants very big image -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimageverybig']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '23:9' => [
                        'title' => 'Querformat (23:9)',
                        'value' => 23 / 9
                    ],
                    '9:23' => [
                        'title' => 'Hochformat (9:23)',
                        'value' => 9 / 23
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '13:5' => [
                        'title' => 'Querformat (13:5)',
                        'value' => 13 / 5
                    ],
                    '5:13' => [
                        'title' => 'Hochformat (5:13)',
                        'value' => 5 / 13
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '2:1' => [
                        'title' => 'Querformat (1:2)',
                        'value' => 2 / 1
                    ],
                    '1:2' => [
                        'title' => 'Hochformat (2:1)',
                        'value' => 1 / 2
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
        ]
    ]
];

// Crop variants big image -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimagebig']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '5:2' => [
                        'title' => 'Querformat (5:2)',
                        'value' => 5 / 2
                    ],
                    '2:5' => [
                        'title' => 'Hochformat (2:5)',
                        'value' => 2 / 5
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '22:9' => [
                        'title' => 'Querformat (22:9)',
                        'value' => 22 / 9
                    ],
                    '9:22' => [
                        'title' => 'Hochformat (9:22)',
                        'value' => 9 / 22
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '7:3' => [
                        'title' => 'Querformat (7:3)',
                        'value' => 7 / 3
                    ],
                    '3:7' => [
                        'title' => 'Hochformat (7/3)',
                        'value' => 3 / 7
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
        ]
    ]
];

// Crop variants medium image -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimagemedium']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '17:7' => [
                        'title' => 'Querformat (17:7)',
                        'value' => 17 / 7
                    ],
                    '7:17' => [
                        'title' => 'Hochformat (7:17)',
                        'value' => 7 / 17
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '9:4' => [
                        'title' => 'Querformat (9:4)',
                        'value' => 9 / 4
                    ],
                    '4:9' => [
                        'title' => 'Hochformat (4:9)',
                        'value' => 4 / 9
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '7:3' => [
                        'title' => 'Querformat (7:3)',
                        'value' => 7 / 3
                    ],
                    '3:7' => [
                        'title' => 'Hochformat (7/3)',
                        'value' => 3 / 7
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
        ]
    ]
];


// Crop variants small image -----------------------------------------------------------------------

$GLOBALS['TCA']['tt_content']['types']['iwansonimagesmall']['columnsOverrides']['assets']['config']['overrideChildTca']['columns']['crop'] = [
    'config' => [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'Desktop',
                'allowedAspectRatios' => [
                    '7:3' => [
                        'title' => 'Querformat (7:3)',
                        'value' => 7 / 3
                    ],
                    '3:7' => [
                        'title' => 'Hochformat (3:7)',
                        'value' => 3 / 7
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'tablet' => [
                'title' => 'Tablet',
                'allowedAspectRatios' => [
                    '7:3' => [
                        'title' => 'Querformat (7:3)',
                        'value' => 7 / 3
                    ],
                    '3:7' => [
                        'title' => 'Hochformat (3:7)',
                        'value' => 3 / 7
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
            'mobile' => [
                'title' => 'Mobil',
                'allowedAspectRatios' => [
                    '7:3' => [
                        'title' => 'Querformat (7:3)',
                        'value' => 7 / 3
                    ],
                    '3:7' => [
                        'title' => 'Hochformat (3:7)',
                        'value' => 3 / 7
                    ],
                    'NaN' => [
                        'title' => 'LLL:EXT:lang/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                ],
            ],
        ]
    ]
];


$ttContentColumns = [
'iwansonChangeColumnOrder' => [
    'exclude' => true,
    'label' => 'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:tt_content.iwansonChangeColumnOrder',
    'config' => [
        'type' => 'check',
        'renderType' => 'checkboxToggle',
        'items' => [
            0 => [
                0 => 0,
                1 => 1
            ]            
        ]
    ]
  ],
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $ttContentColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'iwansonChangeColumnOrder'
);