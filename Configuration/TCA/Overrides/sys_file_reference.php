<?php
     
$fileReferenceColumns = [
    'iwansonsstuffdisquieter' => [
    'exclude' => true,
    'label' => 'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:sys_file_reference.iwansonsstuffdisquieter',
    'config' => [
        'type' => 'text',
        'enableRichtext' => true,
        'richtextConfiguration' => 'default',
        'cols' => 20,
        'rows' => 5,
        'eval' => 'null',
        'default' => null,
    ]
  ],
//    'iwansonsstuffdisquieter2' => [
//    'exclude' => true,
//    'label' => 'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:sys_file_reference.iwansonsstuffdisquieter2',
//    'config' => [
//        'type' => 'text',
//        'enableRichtext' => true,
//        'richtextConfiguration' => 'default',
//        'cols' => 20,
//        'rows' => 5,
//        'eval' => 'null',
//        'default' => null,
//    ]
//  ],  
 'iwansonsstufflinktext' => [
    'exclude' => true,
    'label' => 'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:sys_file_reference.iwansonsstufflinktext',
    'config' => [
        'type' => 'input',
        'eval' => 'null',
        'default' => null,
    ]      
  ]
   
];

$fileReferenceColumns2 = [
'iwansonsstuffimagetext' => [
    'exclude' => false,
    'label' => 'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:sys_file_reference.iwansonsstuffimagetext',
    'config' => [
        'type' => 'text',
        'enableRichtext' => true,
        'richtextConfiguration' => 'default',
        'cols' => 20,
        'rows' => 5,
        'eval' => 'null',
        'default' => null,
    ]
  ],
];   
$fileReferenceColumns3 = [
 'iwansonsstuffpreviewImage' => [
    'exclude' => true,
    'label' => 'LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:sys_file_reference.iwansonsstuffpreviewImage',
           'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'columns' => [
                            'crop' => [
                                
                                
                                'config' => [
                                    'type' => 'imageManipulation',
                                    'cropVariants' => [
                                        'default' => [
                                            'title' => 'Desktop',
                                            'allowedAspectRatios' => [
                                                '3:7' => [
                                                    'title' => 'Kalender-Hoch-Format (3:7)',
                                                    'value' => 3/7
                                                ],
                                            ],
                                        ],
                                        'tablet' => [
                                            'title' => 'Tablet',
                                            'allowedAspectRatios' => [
                                                '7:16' => [
                                                    'title' => 'Kalender-Hoch-Format (7:16)',
                                                    'value' => 7/16
                                                ],
                                            ],
                                        ],
                                    ]
                                ]                                

                            ],
                        ],
                    ],                    
                ],
            ),
     ] 
];   
// addToAllTCAtypes ($table, $newFieldsString, $typeList='', $position='')



\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $fileReferenceColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $fileReferenceColumns2);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'sys_file_reference',
    'imageoverlayPalette',
    'iwansonsstuffimagetext, --linebreak--, iwansonsstuffdisquieter, iwansonsstufflinktext, --linebreak--',
    'before:crop'    
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $fileReferenceColumns3);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'sys_file_reference',
    'videoOverlayPalette',
    'iwansonsstuffpreviewImage, --linebreak--',
    'before:autoplay'    
);

