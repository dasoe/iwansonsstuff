
mod.wizards.newContentElement.wizardItems.common{
    elements{
        startpageslider {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:startpageslider.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:startpageslider.description
            tt_content_defValues{
                CType = startpageslider
            }
        }
    }
    show:= addToList(startpageslider)
}
    
