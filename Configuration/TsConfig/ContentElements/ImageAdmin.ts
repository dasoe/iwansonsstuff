
# this is a first-Level-Element. Meaning that it is not contained by any container.
# if a header is used, it shall become point in middle menu. grid elements type was
# added in where clause of menu in setup!

mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimageadmin {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimageadmin.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimageadmin.description
            tt_content_defValues{
                CType = iwansonimageadmin
            }
        }
    }
    show:= addToList(iwansonimageadmin)
}
    
