
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimagetiles {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagetiles.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagetiles.description
            tt_content_defValues{
                CType = iwansonimagetiles
            }
        }
    }
    show:= addToList(iwansonimagetiles)
}
    
