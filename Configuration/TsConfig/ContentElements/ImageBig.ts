
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimagebig {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imagebig.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imagebig.description
            tt_content_defValues{
                CType = iwansonimagebig
            }
        }
    }
    show:= addToList(iwansonimagebig)
}
    
