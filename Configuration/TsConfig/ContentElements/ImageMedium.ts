
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimagemedium {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imagemedium.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imagemedium.description
            tt_content_defValues{
                CType = iwansonimagemedium
            }
        }
    }
    show:= addToList(iwansonimagemedium)
}
    
