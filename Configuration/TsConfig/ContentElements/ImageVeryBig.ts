
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimageverybig {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imageverybig.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imageverybig.description
            tt_content_defValues{
                CType = iwansonimageverybig
            }
        }
    }
    show:= addToList(iwansonimageverybig)
}
    
