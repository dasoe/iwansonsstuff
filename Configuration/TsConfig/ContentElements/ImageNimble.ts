
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimagenimble {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagenimble.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagenimble.description
            tt_content_defValues{
                CType = iwansonimagenimble
            }
        }
    }
    show:= addToList(iwansonimagenimble)
}
    
