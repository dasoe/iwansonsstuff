
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimagebrochure {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagebrochure.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:iwansonimagebrochure.description
            tt_content_defValues{
                CType = iwansonimagebrochure
            }
        }
    }
    show:= addToList(iwansonimagebrochure)
}
    