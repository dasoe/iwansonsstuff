
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonfixedbutton {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:fixedbutton.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:fixedbutton.description
            tt_content_defValues{
                CType = iwansonfixedbutton
            }
        }
    }
    show:= addToList(iwansonfixedbutton)
}
    
