
mod.wizards.newContentElement.wizardItems.common{
    elements{
        iwansonimagesmall {
            iconIdentifier = content-image
            title = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imagesmall.title
            description = LLL:EXT:iwansonsstuff/Resources/Private/Language/locallang_db.xlf:imagesmall.description
            tt_content_defValues{
                CType = iwansonimagesmall
            }
        }
    }
    show:= addToList(iwansonimagesmall)
}
    
