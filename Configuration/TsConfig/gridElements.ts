
tx_gridelements.setup {


    cols-17 {
        title = Voll: 17
        icon = EXT:gridelements/Resources/Public/Icons/gridelements.svg
         config {
            colCount = 1
            rowCount = 1
            rows.1.columns {
                1 {
                  name = voll
                  colPos = 0
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                }
            }
        }
    }


    cols-12 {
        title = 1 Spalte: (3) 12 (2)
        icon = EXT:gridelements/Resources/Public/Icons/gridelements.svg
         config {
            colCount = 1
            rowCount = 1
            rows.1.columns {
                1 {
                  name = mitte breit
                  colPos = 0
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
            }
        }
    }


    cols-3-6-6 {
        title = 2 Spalten: (3) 6-6  
        icon = EXT:gridelements/Resources/Public/Icons/gridelements.svg
         config {
            colCount = 3
            rowCount = 1
            rows.1.columns {
                1 {
                  name = links (schmal) 
                  colPos = 0
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 1
                }
                2 {
                  name = rechts (schmal)
                  colPos = 2
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 1
                }
            }
        }
    }

    cols-6-8 {
        title = 2 Spalten: 6 (3) 8
        icon = EXT:gridelements/Resources/Public/Icons/gridelements.svg
         config {
            colCount = 2
            rowCount = 1
            rows.1.columns {
                1 {
                  name = links (schmal)
                  colPos = 0
                  allowed.CType = list,text,textpic,image,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
                2 {
                  name = rechts (breit)
                  colPos = 2
                  allowed.CType = list,text,textpic,image,textmedia,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
            }
        }
    }

    cols-8-8 {
        title = 2 Spalten > 2 Zeilen: 8-8
        icon = EXT:gridelements/Resources/Public/Icons/gridelements.svg
         config {
            colCount = 2
            rowCount = 1
            rows.1.columns {
                1 {
                  name = links
                  colPos = 0
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
                2 {
                  name = rechts
                  colPos = 2
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
            }
        }
    }

    cols-6-10 {
        title = 2 Spalten > 2 Zeilen: 6 (1) 10
        description = 2 Spal
        icon = EXT:gridelements/Resources/Public/Icons/gridelements.svg
         config {
            colCount = 2
            rowCount = 1
            rows.1.columns {
                1 {
                  name = links (schmal)
                  colPos = 0
                  allowed.CType = text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
                2 {
                  name = rechts (breit)
                  colPos = 2
                  allowed.CType = list,text,mobiledistance,iwansonimagesmall,iwansonimagemedium,iwansonimagebig,iwansonimageverybig,iwansonfixedbutton,iwansonimagenimble,iwansonimagebrochure                  
                  maxitems = 3
                }
            }
        }
    }


}