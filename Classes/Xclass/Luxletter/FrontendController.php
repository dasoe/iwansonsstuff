<?php
declare(strict_types=1);
namespace Dasoe\Iwansonsstuff\Xclass\Luxletter;

use In2code\Luxletter\Domain\Model\Newsletter;
use In2code\Luxletter\Domain\Model\User;
use In2code\Luxletter\Domain\Model\Usergroup;
use In2code\Luxletter\Domain\Repository\UsergroupRepository;
use In2code\Luxletter\Domain\Repository\UserRepository;
use In2code\Luxletter\Domain\Service\LogService;
use In2code\Luxletter\Domain\Service\ParseNewsletterUrlService;
use In2code\Luxletter\Exception\ArgumentMissingException;
use In2code\Luxletter\Exception\AuthenticationFailedException;
use In2code\Luxletter\Exception\MissingRelationException;
use In2code\Luxletter\Exception\UserValuesAreMissingException;
use In2code\Luxletter\Utility\BackendUserUtility;
use In2code\Luxletter\Utility\LocalizationUtility;
use In2code\Luxletter\Utility\ObjectUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;

/**
 * Class FrontendController
 */
class FrontendController extends \In2code\Luxletter\Controller\FrontendController
{

    // see https://github.com/in2code-de/luxletter/pull/18/files
    
    /**
     * @param User|null $user
     * @param Newsletter $newsletter
     * @param string $hash
     * @return void
     */
    public function unsubscribeAction(User $user = null, Newsletter $newsletter = null, string $hash = ''): void
    {
        try {
            $this->checkArgumentsForUnsubscribeAction($user, $newsletter, $hash);
            /** @var Usergroup $usergroupToRemove */
            //$usergroupToRemove = $this->usergroupRepository->findByUid((int)$this->settings['removeusergroup']);
            $usergroupToRemove = $newsletter->getReceiver();
            $user->removeUsergroup($usergroupToRemove);
            $this->userRepository->update($user);
            $this->userRepository->persistAll();
            $this->view->assignMultiple([
                'success' => true,
                'user' => $user,
                'hash' => $hash,
                'usergroupToRemove' => $usergroupToRemove
            ]);
            if ($newsletter !== null) {
                $this->logService->logUnsubscribe($newsletter, $user);
            }
        } catch (\Exception $exception) {
            $languageKey = 'fe.unsubscribe.message.' . $exception->getCode();
            $message = LocalizationUtility::translate($languageKey);
            $this->addFlashMessage(($languageKey !== $message) ? $message : $exception->getMessage());
        }
    }
    
    /**
     * @param User|null $user
     * @param Newsletter|null $newsletter
     * @param string $hash
     * @return void
     * @throws ArgumentMissingException
     * @throws AuthenticationFailedException
     * @throws MissingRelationException
     * @throws UserValuesAreMissingException
     */
    protected function checkArgumentsForUnsubscribeAction(
        User $user = null,
        Newsletter $newsletter = null,
        string $hash = ''
    ): void {
        if ($user === null) {
            throw new ArgumentMissingException('User not given', 1562050511);
        }
        if ($newsletter === null) {
            throw new ArgumentMissingException('Newsletter not given', 1562267031);
        }
        if ($hash === '') {
            throw new ArgumentMissingException('Hash not given', 1562050533);
        }
        //$usergroupToRemove = $this->usergroupRepository->findByUid((int)$this->settings['removeusergroup']);
        $usergroupToRemove = $newsletter->getReceiver();
        if ($user->getUsergroup()->contains($usergroupToRemove) === false) {
            throw new MissingRelationException('Usergroup not assigned to user', 1562066292);
        }
        if ($user->getUnsubscribeHash() !== $hash) {
            throw new AuthenticationFailedException('Given hash is incorrect', 1562069583);
        }
    }    
}