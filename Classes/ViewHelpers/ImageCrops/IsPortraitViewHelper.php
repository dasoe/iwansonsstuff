<?php

namespace Dasoe\Iwansonsstuff\ViewHelpers\ImageCrops;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 */

/**
 * Class IsPassedViewHelper
 */
class IsPortraitViewHelper extends AbstractViewHelper {

    public function initializeArguments() {
        $this->registerArgument('cropString', 'string', 'crop string', true);
        $this->registerArgument('index', 'integer', 'index of crop variant', true);
    }

    public static function renderStatic(
            array $arguments,
            \Closure $renderChildrenClosure,
            RenderingContextInterface $renderingContext
    ) {


        $isPortrait = false;

        $contents = getContents(htmlspecialchars_decode($arguments['cropString']), '"selectedRatio":"', '","focusArea"');
        //DebuggerUtility::var_dump($contents);
        //DebuggerUtility::var_dump($contents[$arguments['index']]);
        $actualCropRatioArray = explode(':', $contents[$arguments['index']]);
        if ( intval($actualCropRatioArray[0]) < intval($actualCropRatioArray[1]) ) {
            $isPortrait = true;
        }

        return $isPortrait;
    }

}

function getContents($str, $startDelimiter, $endDelimiter) {
    $contents = array();
    $startDelimiterLength = strlen($startDelimiter);
    $endDelimiterLength = strlen($endDelimiter);
    $startFrom = $contentStart = $contentEnd = 0;
    while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
        $contentStart += $startDelimiterLength;
        $contentEnd = strpos($str, $endDelimiter, $contentStart);
        if (false === $contentEnd) {
            break;
        }
        $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
        $startFrom = $contentEnd + $endDelimiterLength;
    }

    return $contents;
}
