<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/gridElements.ts">');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/StartpageSlider.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageTiles.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageAdmin.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/MobileDistance.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageInstructor.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageGuestInstructor.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageNimble.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageBrochure.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/FixedButton.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageVeryBig.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageBig.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageMedium.ts">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:typo3conf/ext/iwansonsstuff/Configuration/TsConfig/ContentElements/ImageSmall.ts">');
// Individuelle RTE-Konfiguration registrieren in der ext_localconf.php
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['cs_presets'] = 'EXT:iwansonsstuff/Configuration/RTE/CsPresets.yaml';
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['nl_presets'] = 'EXT:iwansonsstuff/Configuration/RTE/NewsletterPresets.yaml';
$GLOBALS["TYPO3_CONF_VARS"]["SYS"]["FileInfo"]["fileExtensionToMimeType"]["mp4"] = "video/mp4";

