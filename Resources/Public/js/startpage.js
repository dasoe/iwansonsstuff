/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    console.log("starpage.js loaded. start...");
    oetext = $('#ticker').data("ticker");
    console.log(oetext);
    letterIndex = 0;
    
     
    addPadding = parseInt( $('#ticker').css("line-height").replace("px","") );

    // now get all the transformations done
    adjustAllStartpage();
    // do the same in case window is resized
    $(window).resize(function () {
        adjustAllStartpage();
    });

    tickerLoop();


});

function tickerLoop () {
  let wait = parseInt( Math.random()*100 ) + 20;
  setTimeout(function () {
      if (oetext.charAt(letterIndex) == "\n") {
          $('#ticker').append( " | " );
            setTimeout(function () {
            }, 50);
          if ( $('#ticker').height() + addPadding - window.innerHeight > 0 ) {
               $('#ticker'). css ( "top", parseInt( window.innerHeight - $('#ticker').height() -addPadding ) );
          }

      } else {
          $('#ticker').append( oetext.charAt(letterIndex) );
          if ( $('#ticker').height() + addPadding - window.innerHeight > 0 ) {
               $('#ticker'). css ( "top", parseInt( window.innerHeight - $('#ticker').height() -addPadding ) );
          }
      }
      letterIndex++;
      if (letterIndex > oetext.length) {
    letterIndex = 0;
      }
      tickerLoop();       // Call the loop again, and pass it the current value of i
    }, wait);
};


function adjustAllStartpage() {
    //startpageDisquieterButton
//    oneUnity = $('#wrap').width() / 48;
//    onePixel = oneUnity / 40;

    if (window.innerWidth > 1024) {
// ###############################################################################################################################
// ###  DESKTOP  #################################################################################################################
// ###############################################################################################################################

        $('#sliderOeWrapper').css('width', oUnt * 45);
        $('.swiper-slide').css('background-size', oUnt * 45);
        $('.swiper-button-next').css('cssText', 'width: ' + (oUnt * 3) + 'px !important');
        $('.swiper-button-prev').css('cssText', 'width: ' + (oUnt * 3) + 'px !important');

        
        $('.startpageDisquieterButton').css('width', oPx * 60);
        $('.startpageDisquieterButton').css('height', oPx * 90);

        $('.startpageDisquieter').css({
            'left': oUnt * 15,
            'top': oUnt * 17.5,
        });
        $('.startpageDisquieter .content').css({
            'left': oUnt * 0.625,
            'top': - oUnt * 1.75,
        });
        
        $('.startpageDisquieter2').css({
            'left': oUnt * 38,
            'bottom': oUnt * 2.5,
        });
        $('.startpageDisquieter2Button').css({
            'width': oUnt * 4,
            'height': oUnt * 1.5,
        });
        $('.startpageDisquieter2 .content').css({
            'left': oUnt * 0.625,
            'top': - oUnt * 1.25,
        });

        
// ###############################################################################################################################
// ###  TABLET  ##################################################################################################################
// ###############################################################################################################################
    } else if (window.innerWidth > 440 && window.innerWidth < 1025) {


        $('#sliderOeWrapper').css('width', oUnt * 24);
        $('.swiper-slide').css('background-size', oUnt * 24);
        $('.swiper-button-next').css('cssText', 'width: ' + (oUnt * 3) + 'px !important');
        $('.swiper-button-prev').css('cssText', 'width: ' + (oUnt * 3) + 'px !important');

        $('.startpageDisquieterButton').css('width', oPx * 40);
        $('.startpageDisquieterButton').css('height', oPx * 60);

        $('.startpageDisquieter').css({
            'left': oUnt * 6.25,
            'top': oUnt * 12,
        });
        $('.startpageDisquieter .content').css({
            'left': oUnt * 0.625,
            'top': - oUnt ,
        });
        
        $('.startpageDisquieter2').css({
            'left': oUnt * 17,
            'bottom': oUnt * 1,
        });
        $('.startpageDisquieter2Button').css({
            'width': oUnt * 3,
            'height': oUnt * 1,
        });
        $('.startpageDisquieter2 .content').css({
            'left': oUnt * 0.625,
            'top': - oUnt * 0.9,
        });


// ###############################################################################################################################
// ###  MOBILE  ##################################################################################################################
// ###############################################################################################################################        
    } else {
        
        $('#sliderOeWrapper').css('width', oUnt * 11);
        $('.swiper-slide').css('background-size', oUnt * 11);
        $('.swiper-button-next').css('cssText', 'width: ' + (oUnt * 3) + 'px !important');
        $('.swiper-button-prev').css('cssText', 'width: ' + (oUnt * 3) + 'px !important');

        $('.startpageDisquieterButton').css('width', oPx * 30);
        $('.startpageDisquieterButton').css('height', oPx * 45);

        $('.startpageDisquieter').css({
            'left': oUnt,
            'top': oUnt *9,
        });
        $('.startpageDisquieter .content').css({
            'left': oUnt * 0.5,
            'top': - oUnt/1.5 ,
        });
        
        $('.startpageDisquieter2').css({
            'left': oUnt * 4,
            'bottom': oUnt * 1,
        });
        $('.startpageDisquieter2Button').css({
            'width': oUnt * 2.5,
            'height': oUnt * 0.75,
        });
        $('.startpageDisquieter2 .content').css({
            'left': oUnt * 0.625,
            'top': - oUnt * 0.75,
        });
    }        
}