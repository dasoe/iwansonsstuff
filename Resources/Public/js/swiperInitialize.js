/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

  $(document).ready(function () {
    //initialize swiper when document ready
    
    //console.log("swiper initialized");
    let number = $('.swiper-slide').length;
    let firstIndex = Math.floor(Math.random() * number);
    
    
    var mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
      setWrapperSize: true,
      initialSlide: firstIndex,
      centeredSlides: true,
      loop: true,
      effect: 'fade',
      speed: 600,
     autoplay: {
        delay: 80000,  
        disableOnInteraction: false,
     },  
      

 
    });
    
    mySwiper.on('slideChange', function () {
        console.log('slide changed');
            // also, we have to add the (mouse-)listener for the newly created links
            $('.startpageDisquieter a').on("mouseover", function (event, data) {
                $('#mouseReplacement').css({
                    'background-size': '35%'
                });
            });
            $('.startpageDisquieter a').on("mouseout", function (event, data) {
                $('#mouseReplacement').css({
                    'background-size': '100%'
                });
            });            
            $('.startpageDisquieter2 a').on("mouseover", function (event, data) {
                $('#mouseReplacement').css({
                    'background-size': '35%'
                });
            });
            $('.startpageDisquieter2 a').on("mouseout", function (event, data) {
                $('#mouseReplacement').css({
                    'background-size': '100%'
                });
            });    
    
    });
//    
//    mySwiper.on('slideChangeTransitionStart', function () {
//        console.log('slideChangeTransitionStart');
//      });
//    mySwiper.on('slideChangeTransitionEnd', function () {
//        console.log('slideChangeTransitionEnd');
//      });
//      
// why do I need to dot this? Not sure, anyway:
      $('.swiper-button-prev').on('click', function () {
        console.log('clicke prev button');
        mySwiper.slidePrev();
      });
        $('.swiper-button-next').on('click', function () {
            mySwiper.slideNext();
        console.log('clicke next button');
      });
  });