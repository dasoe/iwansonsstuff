/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
String.prototype.truncateBySent = function(sentCount = 3, moreText = "") {
  //match ".","!","?" - english ending sentence punctuation
  var sentences = this.match(/[^\.!\?]+[\.!\?]+/g);
  if (sentences) {
    //console.log(sentences.length);
    if (sentences.length >= sentCount && sentences.length > sentCount) {
      //has enough sentences
      return sentences.slice(0, sentCount).join(" ") + moreText;
    }
  }
  //return full text if nothing else
  return this;
};

// global - as they change on runtime and are used in on click events, defined on page load
var oUnt;
var oPx;
var env;
var lastEnv;
var rightColumnFixedAt;

$(document).ready(function () {

     console.log("ready!");
     console.log(window.devicePixelRatio);
    rightColumnFixedAt = 0;
    // columns in Content are calculated by flex
    // get size of one Unity ( one quad in grid )
    // using columns calculated by flex as a basis
    // oUnt = oneUnit, oPX = onePixel env = environment
    updateUnits();

    // spinner (carfeul: dynamically created links (submenu), so different syntax )
     $('#innerWrap a').on('click', function(event, data) {
         if ( $(this).attr("href") ) {
             if ( !$(this).attr("target") == "_blank" ) {
                    showSpinner(event);
             }
        }
     });
     $('ul.menuLevel1 li').on('click', 'a', function(event, data) {
        showSpinner(event);
     });
    

    // middle menu (scroll to container)
    $(".containerLink").on("click", function (event, data) {
         console.log("clicked on container link. Scrolling...");
        let elementId = $(this).attr('id').replace("containerLink", "container");
         console.log("jump To Element " + elementId);
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#" + elementId).offset().top - (oUnt * 4)
        }, 1400);
    });


            
    // prepare footer
    $('#footerIcon').click(function ()
    {
        let IconObject = $(this);
        if (!IconObject.hasClass("open") && !IconObject.hasClass("opening")) {
            openFooter(IconObject);
        } else {
            IconObject.removeClass("opening");
            IconObject.removeClass("open");
            IconObject.delay(30).animate({
                bottom: oUnt * 1,
            }, 550, function () {
                // Animation complete.
            });

            $('#footerItself').animate({
                height: 0,
                opacity: 0
            }, 650, function () {
            });
        }
    });
    
    $('#footerMenuCloseButton').click(function ()
    {
        $('#footerIcon').click();
    });
    
    // open footer
    let IconObject = $('#footerIcon');
    if (!IconObject.hasClass("open") && !IconObject.hasClass("opening")) {
       // openFooter(IconObject);
    }

    $("#hamburger").on("click", function (event, data) {
         console.log("Opening Menu via Hamburger...");
        $("#menuCloseButton").show();

        $('#menuInner').css({
            'display':'block'
        });
        $('#menuInner').delay(600).velocity({
            'opacity': 1,
        }, 500, function () {
        });        
        
        $("#menu").velocity({
            'height': '100%',
            'paddingTop': oUnt * 1.5
                    //borderLeftColor: '#fff',
        }, 500, function () {
            // Animation complete.
            $("#langMenu").show();
        });
    });

    $("#menuOpenButton").on("click", function () {
        //$("#menuOpenButton").on("click", function (event, data) {
         console.log("Opening Menu...");
        $("#menu").addClass('opening');
        $("#menuCloseButton").show();
        $("#menuRotate").hide();
        $("#langMenu").hide();
        
        $('#menuInner').css({
            'display':'block'
        });
        $('#menuInner').delay(600).velocity({
            'opacity': 1,
        }, 500, function () {
        });        
        
        $("#menu").animate({
            //borderLeftColor: '#fff',
        }, 500, function () {
            // Animation complete.
        });
        $("#menuInnerWrapper").animate({
            width: (oUnt * 45),
        }, 500, function () {
            $("#menu").addClass('open');
            $("#menu").removeClass('opening');
        });
    });

    $("#menuCloseButton").on("click", function (event, data) {
        $('#menuInner').velocity({
            'opacity': 0,
        }, 200, function () {
            $('#menuInner').css({
                'display':'none'
            });                
        });            
       console.log("Closing Menu...");
        if (env == "mobile") {
            $("#langMenu").hide();
            $("#menu").velocity({
                'height': 0,
                'paddingTop': 0
                        //borderLeftColor: '#fff',
            }, 500, function () {
                // Animation complete.
                $("#menuCloseButton").hide();
                $("#menu").removeClass('open');
            });
        } else {
            $("#menuInnerWrapper").animate({
                width: '0',
            }, 500, function () {
                // Animation complete.
                $("#langMenu").show();
                $("#menuRotate").show();
                $("#menuCloseButton").hide();
                $("#menu").removeClass('open');
            });
        }
    });

    // form file upload
    $('input.lightbox').on("change", function (event, data) {
         console.log("clicking file upload button...");
        let imageName = $(this).val().replace("C:\\fakepath\\", "");

        $(this).next(".oeShowNameOfFile").html(imageName);
    });

    // on page load, if submenu: prepare correct submenu TODO: redundant code!
    if ($('#mainmenu .menuLevel1 li.cur').attr('class')) {
        let pid = parseInt($('#mainmenu .menuLevel1 li.cur').attr('class').replace(/\D/g, ''));
        if ($('ul#menuSubParts li.menuSubPart' + pid).length > 0) {
            let contentToAdd = '<ul id="tempSubMenu">';
            $('ul#menuSubParts li.menuSubPart' + pid).each(function (event, data) {
                //jlda               
                contentToAdd += '<li class="lay'+$(this).data("hlay")+'">';
                contentToAdd += this.innerHTML;
                contentToAdd += "</li>";
            });
            contentToAdd += "</ul>";
            $('#mainmenu .menuLevel1 li.cur').append(contentToAdd);
            $('#mainmenu .menuLevel1 li.cur').addClass("open");
        }
    }

    if (env == "desktop") {
        $('a').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('a').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });
       
         $('button').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('button').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });
        
         $('input').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('input').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        })
       
        
         $('label.lightbox').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('label.lightbox').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        })
        
        $('.iwansonimagetiles .iwansonOneImage').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('.iwansonimagetiles .iwansonOneImage').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });

                
        $('.containerLink').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('.containerLink').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });
        $('#menuOpenButton').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('#menuOpenButton').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });

        $('#menuCloseButton').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('#menuCloseButton').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });
        $('#calCloseButton').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('#calCloseButton').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        });
        
        // footer icon hover
        $('#footerIcon').mouseenter(function ()
        {
            $(this).find("img").attr("src",  $(this).find("img").attr("src").replace("_closed", "_hover") );
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
            
        });

        $('#footerIcon').mouseleave(function ()
        {
            $(this).find("img").attr("src",  $(this).find("img").attr("src").replace("_hover", "_closed") );
            $('#mouseReplacement').css({
                'background-size': '100%'
             });
        });



    // now for something really ugly. Anyway: Menu hovers bring the sub menus (page part to jump to)
        $('#mainmenu .menuLevel1 li').on("mouseover", function (event, data) {
            // strip all non-number
            if (!$(this).hasClass("open")) {
                openSubMenu($(this));
            }

        });
    } else {
        
        
        // marks menu parts: do they have sub menu?
        $('#mainmenu .menuLevel1 li').each(function (index, el) {
            let pid = parseInt($(this).attr('class').replace(/\D/g, ''));
            if ($('ul#menuSubParts li.menuSubPart' + pid).length > 0) {
                $(this).addClass('hasSub');
            }
        });

    }
    // open / close submenu on click - in case it is open that is
    $('#mainmenu .menuLevel1 li a').on("click", function (event, data) {
         console.log("clicked a 'li a'");
        
        if (!$(this).parent().hasClass("open")) {            
            if ($(this).parent().hasClass("hasSub")) {
                event.preventDefault();
                 console.log("no class 'open'");
                openSubMenu($(this).parent());
            }
        } else {
             console.log("has class 'open'");            
            if (env != "Desktop") {
             console.log("is mobile");
                
                $(this).parent().removeClass("open");
                $("ul#tempSubMenu").remove();
            }
        }
    });

    // admin
    $('.oneAdmin').on("click", function (event, data) {
         console.log("Clicked on Admin...");
        let uid = $(this).data("uidnr");
        let nummer = $(this).data("adminnr");
        setActiveClassesForAdmin( uid, nummer );                
        closeAllAdminsTexts(uid);
        openAdminText( $(this) );
        
    });

    $('.iwansonOneImage.admin').on("click", function (event, data) {
         console.log("Clicked on Admin Image...");
        let uid = $(this).data("uidnr");
        let nummer = $(this).data("adminnr");
        setActiveClassesForAdmin( uid, nummer );
        closeAllAdminsTexts(uid);
        openAdminText( $('.oneAdmin.uidnr' + uid + '.adminnr' + nummer) );

    });

    // instructor
    $('.oneInstructor.main').on("click", function (event, data) {
         console.log("Clicked on Instructor...");
        let uid = $(this).data("uidnr");
        let nummer = $(this).data("instructornr");
        $('.oneInstructor.uidnr' + uid).removeClass("active");
        $('.iwansonOneImage.instructor.uidnr' + uid).css({
            'opacity': 0
        });
        $(this).addClass("active");
        $('.iwansonOneImage.instructor.uidnr' + uid + '.instructornr' + nummer).css({
            'opacity': 1
        });

//        $('.instructorDescriptionContainer.uidnr' + uid).html($('.instructorDescriptionHolder.uidnr' + uid + '.instructornr' + nummer).html());
//        $('.instructorDescriptionContainer.uidnr' + uid).css({
//            'height': 'auto',
//            'opacity': 1
//        });
        closeAllInstructorsTexts(uid);
        openInstructorText( $(this) );

    });
    
       // instructor
    $('.oneInstructor.guest').on("click", function (event, data) {
         console.log("Clicked on Instructor...");
        let uid = $(this).data("uidnr");
        let nummer = $(this).data("instructornr");
        $('.oneInstructor.uidnr' + uid).removeClass("active");
        $('.iwansonOneImage.instructor.uidnr' + uid).css({
            'opacity': 0
        });
        $(this).addClass("active");
        $('.iwansonOneImage.instructor.uidnr' + uid + '.instructornr' + nummer).css({
            'opacity': 1
        });

        $('.instructorDescriptionContainer.uidnr' + uid).html($('.instructorDescriptionHolder.uidnr' + uid + '.instructornr' + nummer).html());
        $('.instructorDescriptionContainer.uidnr' + uid).css({
            'height': 'auto',
            'opacity': 1
        });

    });
//
//    $('.iwansonOneImage.instructor').on("click", function (event, data) {
//        console.log("Clicked on Instructor Image...");
//        let uid = $(this).data("uidnr");
//        let nummer = $(this).data("instructornr");
//        $('.oneInstructor.uidnr' + uid).removeClass("active");
//        $('.iwansonOneImage.instructor.uidnr' + uid).removeClass("active");
//        $(this).addClass("active");
//        $('.oneInstructor.uidnr' + uid + '.instructornr' + nummer).addClass("active");
////        $('.instructorDescriptionContainer.uidnr' + uid).html($('.instructorDescriptionHolder.uidnr' + uid + '.instructornr' + nummer).html());
////        $('.instructorDescriptionContainer.uidnr' + uid).css({
////            'height': 'auto',
////            'opacity': 1
////        });
//
//        closeAllInstructorsTexts(uid);
//        openInstructorText( $(this) );
//
//    });
//

//    $('.iwansonImage').mouseenter(function ()
//    {
//        $(this).css('-webkit-filter', 'grayscale(0%) contrast(100%)');
//        $(this).css('filter', 'grayscale(0%) contrast(100%)');
//    });
//
//    $('.iwansonImage').mouseleave(function ()
//    {
//        $(this).css('-webkit-filter', 'grayscale(100%) contrast(110%)');
//        $(this).css('filter', 'grayscale(100%) contrast(110%)');
//
//    });



    $(window).scroll(function (event) {
        // console.log("Scrolling...");
        // on scroll: check in which area we are and set the underlining accordingly.  
        if (env != "mobile") {
            checkRightColumFix();
        }
        checkMiddleMenu(areaArray);
    });

    customSelect();


                
        
         $('.custom-select .select-items div').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('.custom-select .select-items div').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        })

         $('.custom-select div.select-selected').on("mouseover", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '35%'
            });
        });
        $('.custom-select div.select-selected').on("mouseout", function (event, data) {
            $('#mouseReplacement').css({
                'background-size': '100%'
            });
        })
    // now get all the transformations done
    adjustAll();

    // middle menu (scroll to container)
    var areaArray = [];
    // prepare areas to jump to
    $($(".oneContainer.layout-0").get().reverse()).each(function (index, el) {
        if ($(this).data("header").length > 0) {
             console.log("found area: " + $(this).data("header"));
            var area = {};
            area["position"] = $(this).position().top;
            area["name"] = $(this).attr('id').replace("container", "containerLink");
            areaArray.push(area);
        }
    });
    console.log (areaArray);
    checkMiddleMenu(areaArray);


    // do the same in case window is resized
    $(window).resize(function () {
        adjustAll();
        checkMiddleMenu(areaArray);

    });


    $('.input.custom-select').next("label.control-label").css({
        'display': 'none',
    });




    function customSelect() {
    // custom select

        var x, i, j, l, ll, selElmnt, a, b, c;
        /* Look for any elements with the class "custom-select": */
        x = document.getElementsByClassName("custom-select");
        l = x.length;
        for (i = 0; i < l; i++) {
            console.log(i);
            selElmnt = x[i].getElementsByTagName("select")[0];
            console.log(selElmnt);
            ll = selElmnt.length;
            /* For each element, create a new DIV that will act as the selected item: */
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            console.log(a);
            
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /* For each element, create a new DIV that will contain the option list: */
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 1; j < ll; j++) {
                /* For each option in the original select element,
                 create a new DIV that will act as an option item: */
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function (e) {
                    /* When an item is clicked, update the original select box,
                     and the selected item: */
                    var y, i, k, s, h, sl, yl;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    sl = s.length;
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < sl; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {

                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            yl = y.length;
                            for (k = 0; k < yl; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                    // oe changed: add a click on the select field for triggering validation re-check
                    s.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function (e) {
                /* When the select box is clicked, close any other select boxes,
                 and open/close the current select box: */
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
    }
    }


    function closeAllSelect(elmnt) {
        /* A function that will close all select boxes in the document,
         except the current select box: */
        var x, y, i, xl, yl, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    
    

    
    if (env == "mobile") {
        if (  $("#oeVars").data("title")!="Impressum" ) {
            adjustMobileOnPageLoad();        
        }
    }

    // now jump to region
    if ($.urlParam('region') > 0) {
        //$('html').css('cursor','none');
        let elementId = "container" + $.urlParam('region');
         console.log("after load: jump to region " + elementId);
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#" + elementId).offset().top - (oUnt * 4)
        }, 1400);
    } else {
        
        var object = $(window.location.hash);
        
        if (object.length > 0) {
            var height = object.offset().top ;
            console.log("no region, but anchor. scroll To: " + height );
            $([document.documentElement, document.body]).animate({
                scrollTop: height - (oUnt * 4)
            }, 800);   
        }

    }



    /* If the user clicks anywhere outside the select box,
     then close all select boxes: */
    document.addEventListener("click", closeAllSelect);








});


function openSubMenu(element) {
    $("ul#tempSubMenu").remove();
    let pid = parseInt(element.attr('class').replace(/\D/g, ''));
     console.log("get subMenus for page id of " + pid);

    $('#mainmenu .menuLevel1 li').removeClass("open");
    if ($('ul#menuSubParts li.menuSubPart' + pid).length > 0) {
        let contentToAdd = '<ul id="tempSubMenu">';
        $('ul#menuSubParts li.menuSubPart' + pid).each(function (event, data) {
            //jlda
             console.log(element);
                contentToAdd += '<li class="lay'+$(this).data("hlay")+'">';
            contentToAdd += this.innerHTML;
            contentToAdd += "</li>";
        });
        contentToAdd += "</ul>";
        element.append(contentToAdd);
        element.addClass("open");
        // as we added it dynamically we have to do it here, not in adjustall(). Duh!
        if (env == "mobile") {
            $('ul#tempSubMenu').css({
                'background-size': oUnt * 7,
                'width': oUnt * 7,
            });
        } else if (env == "tablet") {
            $('ul#tempSubMenu').css({
                'background-size': oUnt * 17,
                'width': oUnt * 17,
            });
        } else {
            $('ul#tempSubMenu').css({
                'background-size': oUnt * 20.75,
                'width': oUnt * 20.75,
            });
            // also, we have to add the (mouse-)listener for the newly created links
            $('ul#tempSubMenu a').on("mouseover", function (event, data) {
                $('#mouseReplacement').css({
                    'background-size': '35%'
                });
            });
            $('ul#tempSubMenu a').on("mouseout", function (event, data) {
                $('#mouseReplacement').css({
                    'background-size': '100%'
                });
            });            
        }
    }
}

/**
 * Check if an element is in the visible viewport
 * @param {jQuery|HTMLElement} el
 * @return boolean
 */
var checkMiddleMenu = function (areaArray) {
    // console.log("check Middle Menu...");
    $('.oneContainer').each(function (event, data) {
        let elementId = $(this).attr('id');
        let linkElementId = elementId.replace("container", "containerLink");
        //console.log ("check " + elementId );

        let elementToUnderline;
        areaArray.every(function (element, index) {
        //console.log ( "Name: " + element.name + " Pos: " + parseInt(element.position));
            elementToUnderline = element.name;
            //console.log(element.name + "-" + element.position + " / " + $(document).scrollTop() );
            if ( ( $(document).scrollTop()  ) > ( parseInt(element.position) )  - (oUnt*10) )  
                return false
            else
                return true
        });
       console.log("underline: " + elementToUnderline);
        $(".containerLink").css('text-decoration', 'none');
        $("#" + elementToUnderline).css('text-decoration', 'underline');
    });

}



var checkRightColumFix = function () {
    // also bad: we check specific containers...
    let rightCol = $('#container13 .contentRight');
    let theBottomcontainer = $('#container283');
    if (rightCol.length && theBottomcontainer.length) {

        let bottomContainerTopVal = $(document).scrollTop() - theBottomcontainer.offset().top + window.innerHeight;
        let bottomVal = rightCol.offset().top + rightCol.height() - $(document).scrollTop() - window.innerHeight + oUnt * 2;

        //console.log( bottomVal );
        if (rightColumnFixedAt == 0) {
            // right column is not fixed
            if (bottomVal < 0) {
                 console.log('now fixxxx it, as we are evil!');
                // now THIS is eeeeeevil. Anyway: Fix a div at ists flex-calculated position
                 console.log('bad idea: do fix right column on position: ' + rightCol.offset().top);
                rightCol.css({
                    'color': '#fff',
                    'z-index':0
                });
                rightColumnFixedAt = -$(document).scrollTop() + rightCol.offset().top -bottomVal;
               $('#fixContainerRightColumn').html(rightCol.html());
                $('#fixContainerRightColumn').css({
                    'width': rightCol.width(),
                    'left': rightCol.offset().left,
                    'top': rightColumnFixedAt,
                    'opacity': 1,
                    'z-index':6
                    
                });
            }


        } else {
            if (bottomVal > 0) {
                 console.log('now de-fix it again, going back to normal...');
                rightCol.css({
                    'color': '#000',
                });
                $('#fixContainerRightColumn').html(rightCol.html());
                $('#fixContainerRightColumn').css({
                    'width': rightCol.width(),
                    'left': rightCol.offset().left,
                    'top': -$(document).scrollTop() + rightCol.offset().top,
                    'opacity': 0,
                });
                rightColumnFixedAt = 0;
            }

            if (bottomContainerTopVal > 0) {
                let add = bottomContainerTopVal;
                 console.log('add' + add);
                 console.log('now move it up, as bottom container is moving in...');
                 console.log('to top calc:   ' + (-$(document).scrollTop() + rightCol.offset().top - add));
                $('#fixContainerRightColumn').css({
                    'top': rightColumnFixedAt - add,
                });

            }

        }

    }

}



/**
 * Check if an element is in the visible viewport
 * @param {jQuery|HTMLElement} el
 * @return boolean
 */
var IsInViewport = function (el) {
    if (typeof jQuery === "function" && el instanceof jQuery)
        el = el[0];
    var rect = el.getBoundingClientRect();
    return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
};

 function closeAllInstructorsTexts(uid) {
        $('.oneInstructor.uidnr' + uid + ' .instructorDescriptionHolder').css({
            'height': 0,
            'opacity': 0,
            'padding-top': 0,
            'padding-bottom': 0
        });
 }

 function openInstructorText(obj) {
        obj.find(".instructorDescriptionHolder").css({
            'height': 'auto',
            'opacity': 1,
            'padding-top': oUnt/2,
            'padding-bottom': oUnt/2,
            
        });
 }
 function setActiveClassesForAdmin(uid,nummer) {
        $('.oneAdmin.uidnr' + uid).removeClass("active");
        $('.iwansonOneImage.admin.uidnr' + uid).removeClass("active");
        $('.oneAdmin.uidnr' + uid + '.adminnr' + nummer).addClass("active");
        $('.iwansonOneImage.admin.uidnr' + uid + '.adminnr' + nummer).addClass("active");
 }

 function closeAllAdminsTexts(uid) {
        $('.oneAdmin.uidnr' + uid + ' .adminDescriptionHolder').css({
            'height': 0,
            'opacity': 0,
            'padding-top': 0,
            'padding-bottom': 0
        });
}
 function openAdminText(obj) {
        obj.find(".adminDescriptionHolder").css({
            'height': 'auto',
            'opacity': 1,
            'padding-top': oUnt/2,
            'padding-bottom': oUnt/2,
            
        });
 }
    
 function adjustMobileOnPageLoad() {

    // ############ adjustments mobile just once on page load ###############
    
    // rebuild the containers: content of openable containers are put in one containter
    let space = "<div class=\"spacerMobileChangedContent\"></div>";
    $(".oneContainer").each(function (index, el) {
        let container = $(this);
        let firstBox = container.find(".innerFlexOnlyDesktop:first");
        firstBox.addClass("firstBox");
        container.find(".innerFlexOnlyDesktop").each(function (index, el) {
            if ( !$(this).hasClass("firstBox") && !$(this).find("table").length ) {
                firstBox.append( space );
                firstBox.append( $(this).html() );
                $(this).parent().remove();
//                $(this).css("border-color", "#39af90"); 
            }
        });        
                
        
        // cut the text
        let cutText = firstBox.text().truncateBySent(2);
//        console.log(cutText);
//        console.log(firstBox.text());
        // find the last words before cut should be applied
        let endText = cutText.substring( cutText.length - 52);
        // find the box where the cut is and apply a "[more...]"
        firstBox.children(":contains("+endText+")").each(function (index, el) {
             var text = $(this).text();
             let readon = $("#oeVars").data("readon");
             let addition = "<div class= \"moreLink\">"+readon+"</div>";
             $(this).html( $(this).html().replace(endText, endText+addition) );
             let newLink = $(this).find("div.moreLink");
            // console.log ( $(this) );
            newLink.css({
                "padding-top":oUnt,
                "padding-bottom":oUnt    
            });
            if ( newLink[0] ) {
//                console.log ( newLink.offset().top );
                var relHeight = newLink.offset().top - firstBox.offset().top ;
                if (firstBox.find(".imageVerybigBodytext") ) {
                     console.log("add height for imageVerybigBodytext");
                    relHeight += oUnt*1.5;
                }

                console.log( relHeight );
                
                firstBox.data("originalHeight", firstBox.height());
                firstBox.css({
                    height: relHeight + oUnt,
                    "overflow-y":"hidden"
                });
                newLink.click(function (event) {
                     console.log("clicked");
                    $(this).velocity({
                        height: 0,
                        paddingTop:0,
                        paddingBottom:0,
                        opacity:0
                    });
                    firstBox.velocity({
                        height: firstBox.data("originalHeight") + oUnt,
                    });
                });                
            }
         });   
             
         // and now?...
        
    //console.log( endText );

        //container.css())
    });


//        $(".menuLevel1 li").each(function (index, el) {
//            if ($(this).attr('class')) {
//                let pid = parseInt($(this).attr('class').replace(/\D/g, ''));
//                if ($('ul#menuSubParts li.menuSubPart' + pid).length > 0) {
//                    $(this).find('a').click(function (event) {
//                        event.preventDefault();
//                    });                    
//                }
//            }
//        });
        $('.moreLink').css({
            'padding-top': oUnt ,
            'padding-bottom': oUnt,
        });
}

function updateUnits() {
    // columns in Content are calculated by flex
    // get size of one Unity ( one quad in grid )
    // using columns calculated by flex as a basis

console.log(window.innerWidth);
console.log($( window ).innerWidth());

    $('#oeVars').html($( window ).innerWidth());
    if ($( window ).innerWidth() > 1024) {
        // Desktop
        oUnt = $('#wrap').width() / 48;
        env = "desktop";
    } else if ($( window ).innerWidth() > 440 && $( window ).innerWidth() < 1025) {
        // Tablet
        oUnt = $('#wrap').width() / 25;
        env = "tablet";
    } else {
        // Mobile
        oUnt = $('#wrap').width() / 9;
        env = "mobile";
    }

    // override TODO: DAs ist fragwürdig, oder??
//    if (window.innerHeight > window.innerWidth) {
//        // Mobile
//        oUnt = $('#wrap').width() / 9;
//        env = "mobile";
//    }
     console.log(window.devicePixelRatio);

    oPx = oUnt / 40;
    console.log('oUnt after updating: ' + oUnt + ' - oPx after updating: ' + oPx);

}

function openFooter(ob) {
            ob.addClass("opening");
            if (env == "mobile") {
                ob.delay(30).animate({
                    bottom: window.innerHeight - oUnt,
                }, 550, function () {
                    // Animation complete.
                });
            } else {
                ob.delay(30).animate({
                    bottom: oUnt * 5,
                }, 550, function () {
                    // Animation complete.
                });
            }
            if (env == "mobile") {
                $('#footerItself').animate({
                    height: window.innerHeight-3,
                    opacity: 1
                }, 650, function () {
                    // Animation complete.
                    //opacity wieder herstellen;
                    ob.removeClass("opening");
                    ob.addClass("open");
                });           
            } else {
                $('#footerItself').animate({
                    height: oUnt * 6,
                    opacity: 1
                }, 650, function () {
                    // Animation complete.
                    //opacity wieder herstellen;
                    ob.removeClass("opening");
                    ob.addClass("open");
                });           
            }   
}

function showSpinner(e) {
    
    //alert("yepp");
    //alert ( e.isDefaultPrevented() );
    if ( e.isDefaultPrevented() ) {
        // do nothing
    } else { 
        $("#spinner").css("display","block");
        $("#spinnerCurtain").css("display","block");
    }
}

function adjustAll() {
    //console.log("adjust All");

    if (env != lastEnv) {
         console.log("############################### (re)setting environment! ###################################");
        lastEnv = env;
        $('.swiper-slide').each(function (index, el) {
            let img = $(this).data(env) ;
            $(this).css('background-image', 'url(' + img + ')');
        });
              
      if (env == "desktop") {
          $('html').css('cursor','none');
          $('#mouseReplacement').css('display', 'block');
       } else {
          $('html').css('cursor','auto'); 
          $('#mouseReplacement').css('display', 'none');
       }

    }
        

    updateUnits();

    //console.log('oUnt after adjusting: ' + oUnt);



//console.log('window.innerWidth: ' + window.innerWidth);
    if (env == "desktop") {
// ###############################################################################################################################
// ###  DESKTOP  #################################################################################################################
// ###############################################################################################################################

        // console.log('oPx: ' + oPx);

//        let logoimage = '/typo3conf/ext/iwansonsstuff/Resources/Public/Media/iwanson-logo-desktop.svg';
//        if ($('#logo img').attr('src') != logoimage) {
//            $('#logo img').attr('src', logoimage);
//        }


        $('#spinner').css({
            'width': oUnt * 48,
        });
        // set logo size and position
        $('#logo').css({
            'width': oUnt * 18,
            'left': oUnt * 2,
            'top': oUnt * 2
        });
        $('#logoBackground').css({
            'width': oUnt * 20,
            'height': oUnt * 4,
        });

        $('#menu table tr td').css('height', (oUnt + 40 + 40) / 3);

        // set footer icon size and position
        $('#footerIcon').css({
            'width': oUnt * 1.5,
            'left': oUnt / 2,
            'bottom': oUnt
        });
        // width as wrap - menu - padding left and right
        $('#footerItself').css({
            'width': oUnt * 41,
            'padding-left': oUnt * 4,
        });

        //$('#footer .footerIcon').css('border-width', oUnt / 4);
        // set margins of page itself
        $('#innerWrap').css({
            'margin-left': oUnt * 2,
            'margin-right': oUnt * 5,
            'padding-top': oUnt * 5
        });
        
        
        $('.fixedButtonWrap').css({
            'left': oUnt * 35,
            'top': oUnt * 3,
            'width': oUnt * 4.5,
            'height': oUnt * 1.5,
            'border-width': oUnt / 8
        });

        $('.fixedButton').css({
            'padding-top': oUnt * 0.45,
            'padding-left': oUnt * 0.5,
            'padding-bottom': oUnt * 0.35,
            'width': oUnt * 9.5
                    
        });

        $('.indent9').css({
            'padding-left': oUnt * 9,
        });
//        $('.contentLeft').css('padding-top', oUnt * 5);
//        $('.contentRight').css('padding-top', oUnt * 5);

        // manually set vertical paddings betw. 
        $('.frame-space-before-extra-small').css('margin-top', oUnt * 1);
        $('.frame-space-before-small').css('margin-top', oUnt * 2);
        $('.frame-space-before-medium').css('margin-top', oUnt * 3);
        $('.frame-space-before-large').css('margin-top', oUnt * 4);
        $('.frame-space-before-extra-large').css('margin-top', oUnt * 5);

        $('.frame-space-after-extra-small').css('margin-bottom', oUnt * 1);
        $('.frame-space-after-small').css('margin-bottom', oUnt * 2);
        $('.frame-space-after-medium').css('margin-bottom', oUnt * 3);
        $('.frame-space-after-large').css('margin-bottom', oUnt * 4);
        $('.frame-space-after-extra-large').css('margin-bottom', oUnt * 5);
        $('#menu').css('left', oUnt * 45);

        $('#menuRotate').css({
            'left': oUnt * 44,
            'top': oUnt*2,
        });


        if ($('#menuInnerWrapper').width() > oUnt) {
            $('#menuInnerWrapper').css({
                'width': (oUnt * 45),
            });
        }

        $('#menuInner').css({
            'width': oUnt * 26,
            'padding-left': oUnt * 5,
            'padding-top': oUnt * 4,
            'height':window.innerHeight - (oUnt * 4),
        });
        
        $('#menuInner ul #tempSubMenu').css({
            'width': oUnt*20,
            'background-size': oUnt*20,
        });
        
        
        
        
        
        $('#menuRotate').css({
            'width': oUnt*5,
        });


//        $('#menu table tr td, #menuRotate table tr td').css({
//            'height': oUnt,
//        });
        $('ul.langMenu li').css({
            'padding-right': oPx*14,
        });

        $('td#langMenu').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 18) + 'px'
        });

        $('#menuOpenButton').css({
            'font-size': oUnt*3/4,
        });
        //console.log("count table cloumns: " + $("table tr").first().children().length);


        $("#innerWrap table").each(function (index, el) {
            if ($(this).find("tr").first().children().length == 2) {
               // console.log("zweispaltige Tabelle gefunden!");
                // tabelle enthält in der ersten Zelle span mit Klasse "event"
                if ($(this).find("span").hasClass('tableLeft5')) {
                    $(this).children("tbody:first").children("tr:first").children("td:first").css({
                        'width': oUnt * 5,
                    });
                } else if ($(this).find("span").hasClass('tableLeft7')) {
                    $(this).children("tbody:first").children("tr:first").children("td:first").css("width", oUnt * 7);
                } else {
                    $(this).children("tbody:first").children("tr:first").children("td:first").css("width", oUnt * 6);
                }



            } else if ($(this).find("tr").first().children().length == 3) {
                 console.log("dreispaltige Tabelle gefunden!");
                $(this).children("tbody:first").children("tr:first").children("td:first").css("width", oUnt * 2.5);
                $(this).children("tbody:first").children("tr:first").children("td").eq(1).css("width", oUnt * 4.5);
                $(this).children("tbody:first").children("tr:first").children("td").eq(2).css("width", oUnt * 10);
            }
                $(this).children("tbody").children("tr").children("td").css({
                    'padding-right': oUnt,
                });
                $(this).children("tbody").children("tr").children("td").css({
                    'padding-bottom': oUnt /4,
                });

        });

//        $( "table tr" ).each(function (index, el) {
//            $(this).children(":first").css( "width", oUnt*7 );
//        });
//                
        // Images. Adjust sizes according to Orientation
        $('img.iwansonImage.small').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 6);
            } else {
                $(this).css('width', oUnt * 14);
            }
        });
        $('img.iwansonImage.medium').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 7);
            } else {
                $(this).css('width', oUnt * 17);
            }
        });
        $('img.iwansonImage.big').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 8);
            } else {
                $(this).css('width', oUnt * 20);
            }
        });
        // ## image very big ##
        $('img.iwansonImage.verybig').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 9);
            } else {
                $(this).css({
                    'width': oUnt * 23,
                });
            }
        });
        $('div.iwansonImage.verybig').css({
            'width': oUnt * 20,
        });
        $('div.iwansonOneImagef.verybig').css({
            'width': oUnt * 20,
        });
        // now get the image out of flex item borders. Duh, dynamic!
        // Flex is calculating our widths automatically. Here we have to override it.
        // So get the width value first as calculated by flex, then adjust the stuff 
        // that changes the width. Then apply the width we calculated. C'est la vie!
        $('img.iwansonImage.verybig').each(function (index, el) {
            let columnWidth = Math.round($(this).closest('.innerFlex').width() / oUnt);
            $(this).closest('.innerFlex').css({
                'width': oUnt * columnWidth,
            });
            let columnWidth2 = Math.round($(this).closest('.innerFlexOnlyDesktop').width() / oUnt);
            $(this).closest('.innerFlexOnlyDesktop').css({
                'width': oUnt * columnWidth2,
            });
        });
        // right Column: move image to left
        $('.contentRight img.iwansonImage.verybig').each(function (index, el) {
            $(this).css({
                'left': -oUnt * 6,
            });
            $(this).closest('.contentRight').css({
                'overflow': 'visible',
            });
            $(this).closest('.contentLeft.columnsReversed').css({
                'overflow': 'visible',
            });
        });
        $('.contentLeft img.iwansonImage.verybig').not(('.contentLeft.columnsReversed img.iwansonImage.verybig')).each(function (index, el) {
            $(this).css({
                'left': -oUnt * 2,
            });
            $(this).closest('.contentLeft').css({
                'overflow': 'visible',
            });                        
        });
        $('.contentLeft.columnsReversed img.iwansonImage.verybig').each(function (index, el) {
            $(this).css({
                'left': -oUnt * 6,
            });
            $(this).closest('.contentLeft').css({
                'overflow': 'visible',
            });                        
            
        });

        $('div.imageVerybigBodytext').css({
            'height': oUnt * 2.5,
        });

        $('.instructorBodytext').css({
            'width': oUnt * 8,
        });        
        
        $('.iwansonOneImage.instructor img').not('.iwansonOneImage.instructor.guest img').css('height', oUnt * 20 );
        
        $('div.iwansonImage.instructor').not('div.iwansonImage.instructor.guest').css({
            'height': oUnt*20,
        });   
        // nimble
        $('img.iwansonImage.nimble').css({
            'height': oUnt * 14.5,
        });
        $('.iwansonOneImage.nimble').css({
            'padding-right': (8 * oPx) + 'px'
        });
        $('.iwansonOneImage.nimble.last').css('padding-right', 0);
        $('.nimblenr2').css('top', oUnt * 2);
        $('.nimblenr3').css('top', oUnt / 8 * 5);
        $('.nimblenr4').css('top', oUnt * 2);

        // admin
        $('img.iwansonImage.admin').css({
            'height': oUnt * 14.5,
        });
        $('.iwansonOneImage.admin').css({
            'margin-right': (8 * oPx) + 'px'
        });
        
        $('.adminList').css({
            'padding-top': (3 * oUnt)
        });
        
        
        $('.iwansonOneImage.admin.last').css('padding-right', 0);
        $('.adminnr2').css('top', oUnt * 2);
        $('.adminnr4').css('top', oUnt * 2);


        $('#contentNavigation').css({
            'left': oUnt * 21,
            'width': oUnt * 3
        });

        $('#contentNavigationInner').css({
            'height': oUnt * 1.5,
            'padding-top': oUnt / 2,
            'padding-left': oUnt
        });


        $('#contentNavigationButton').css({
            'width': oUnt * 2,
            'height': oUnt * 3,
            'bottom': oUnt * 2,
            'left': oUnt * 21
        });

        $('a.button').css({
            'width': oUnt * 4,
            'height': oUnt * 1.25,
            'margin-top': oUnt * 2,
            'margin-right': oUnt * 2

        });

        // in case font size shall be adjusted:
        $('body').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 27) + 'px'
        });



        $('h1').css({
            'font-size': parseInt(oPx * 100) + 'px',
            'line-height': parseInt(oPx * 90) + 'px'
        });
        $('h2').css({
            'font-size': parseInt(oPx * 30) + 'px',
            'line-height': parseInt(oPx * 36) + 'px'
        });

//        $('.h2').css({
//            'font-size': parseInt(oPx * 30) + 'px',
//            'line-height': parseInt(oPx * 36) + 'px'
//        });

        $('.font30').css({
            'font-size': parseInt(oPx * 30) + 'px',
            'line-height': parseInt(oPx * 36) + 'px'
        });

        $('.event').css({
            'font-size': parseInt(oPx * 30) + 'px',
            'line-height': parseInt(oPx * 36) + 'px'
        });


        $('.citation').css({
            'font-size': parseInt(oPx * 30) + 'px',
            'line-height': parseInt(oPx * 36) + 'px',
            'padding-bottom': parseInt(oUnt) + 'px'
        });


        $('.nameList').css({
            'font-size': parseInt(oPx * 30) + 'px',
            'line-height': parseInt(oPx * 36) + 'px'
        });
        $('.select-selected').css({
            'font-size': parseInt(oPx * 30) + 'px',
            'line-height': parseInt(oPx * 36) + 'px'
        });
        $('#menuInner').css({
            'font-size':parseInt(oPx * 30) + 'px',
            'line-height':parseInt(oPx * 72) + 'px'
        });

        $('#footerMenuCloseButton').css({
            'top': oUnt,
            'left': oUnt * 39,
        });


        if ($.urlParam('grid') > 0) {
            $('.contentRight').css("background-image", "url('typo3conf/ext/iwansonsstuff/Resources/Public/Media/gridSomeLine.png')");
            $('.contentLeft').css("background-image", "url('typo3conf/ext/iwansonsstuff/Resources/Public/Media/gridSomeLine.png')");
            $('.contentNavigationSpacer').css("background", "#eeeeef");
            $('#iwansonallinonetry-608-fieldset-2').css("background-image", "url('typo3conf/ext/iwansonsstuff/Resources/Public/Media/gridSomeLine.png')");
        }
// ###############################################################################################################################
// ###  TABLET  ##################################################################################################################
// ###############################################################################################################################
    } else if (env == "tablet") {

        if ($.urlParam('grid') > 0) {
            $('.contentRight').css("background-image", "url('typo3conf/ext/iwansonsstuff/Resources/Public/Media/gridSomeLineTablet.png')");
            $('.contentLeft').css("background-image", "url('typo3conf/ext/iwansonsstuff/Resources/Public/Media/gridSomeLineTablet.png')");
            $('.contentNavigationSpacer').css("background", "#eeeeef");
        }

        let wrapWidth = $('#wrap').width();
         console.log('wrap width: ' + wrapWidth + ' - oUnt: ' + oUnt);
//
//        let logoimage = '/typo3conf/ext/iwansonsstuff/Resources/Public/Media/iwanson-logo-tablet.svg';
//        if ($('#logo img').attr('src') != logoimage) {
//            $('#logo img').attr('src', logoimage);
//        }
        // set logo size and position
        $('#logo').css({
            'width': oUnt * 7.5,
            'left': oUnt * 1.5,
            'top': oUnt,
        });

        $('#logoBackground').css({
            'width': oUnt * 11.5,
            'height': oUnt * 2.5,
        });

        $('#menu table tr td').css('height', oUnt);

        $('#menuCloseButton img').css({
            'width': oPx * 25,
        });
        
        $('#menuInner').css({
            'left': oUnt*3,
        });
        $('#menuInner ul #tempSubMenu').css({
            'width': oUnt*17,
            'background-size': oUnt*17,
            'font-size':parseInt(oPx * 14) + 'px',
            'line-height':parseInt(oPx * 25) + 'px',
        });
        $('.cols-6-3-8-III').css({
            'padding-top': oUnt,
        });
        $('#footerContent .cols-6-1-10-II').css({
            'padding-top': oUnt,
        });
        

        // set footer icon size and position
        //$('#footerIcon').width(oUnt * 2);
        //$('#footer').height( oUnt / 2 );
        $('#footerIcon').css({
            'left': oUnt,
            'bottom': oUnt,
            'width': oUnt * 2,
        });

        $('#footerItself').css({
            'width': oUnt * 23,
            'padding-left': oUnt,
        });
        
        //$('#footer .footerIcon').css('border-width', oUnt / 4);
        // set margins of page itself
        $('#innerWrap').css({
            'margin-left': oUnt * 1.5,
            'margin-right': oUnt * 2.5,
            'padding-top': oUnt * 3
            
        });


        $('.fixedButtonWrap').css({
            'left': oUnt * 18,
            'top': oUnt * 2,
            'width': oUnt * 3.5,
            'height': oUnt * 1,
            'border-width': oUnt / 8
        });

        $('.fixedButton').css({
            'padding-top': oUnt * 0.25,
            'padding-left': oUnt * 0.3,
            'padding-bottom': oUnt * 0.2,
        });


        // manually set vertical paddings betw. 
        $('.frame-space-before-extra-small').css('margin-top', oUnt * 0.5);
        $('.frame-space-before-small').css('margin-top', oUnt * 1);
        $('.frame-space-before-medium').css('margin-top', oUnt * 1.5);
        $('.frame-space-before-large').css('margin-top', oUnt * 2);
        $('.frame-space-before-extra-large').css('margin-top', oUnt * 2.5);

        $('.frame-space-after-extra-small').css('margin-bottom', oUnt * 0.5);
        $('.frame-space-after-small').css('margin-bottom', oUnt * 1);
        $('.frame-space-after-medium').css('margin-bottom', oUnt * 1.5);
        $('.frame-space-after-large').css('margin-bottom', oUnt * 2);
        $('.frame-space-after-extra-large').css('margin-bottom', oUnt * 2.5);


        $('.cols-8-8-I-I').css({
            'padding-bottom': oUnt,
        });

        $('#menu').css('left', oUnt * 23);
        $('#menuRotate').css({
            'left': oUnt * 22,
            'top': oUnt*2,
            'width': oUnt*5,
        });

        $('ul.langMenu li').css({
            'padding-right': oPx*14,
        });

        $('td#langMenu').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 18) + 'px'
        });

        $('#menuOpenButton').css({
            'font-size': oUnt*3/4,
        });











        // Images. Adjust sizes according to Orientation
        $('img.iwansonImage.small').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 3);
            } else {
                $(this).css('width', oUnt * 7);
            }
        });
        $('img.iwansonImage.medium').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 4);
            } else {
                $(this).css('width', oUnt * 9);
            }
        });
        $('img.iwansonImage.big').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 4, 5);
            } else {
                $(this).css('width', oUnt * 11);
            }
        });
        $('img.iwansonImage.verybig').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 5);
            } else {
                $(this).css('width', oUnt * 13);
            }
        });
        
        $('div.iwansonImage.verybig').css({
            'width': oUnt * 9,
        });
        $('div.iwansonOneImage.verybig').css({
            'width': oUnt * 9,
        });        
        // now get the image out of flex item borders. Duh, dynamic!
        // Flex is calculating our widths automatically. Here we have to override it.
        // So get the width value first as calculated by flex, then adjust the stuff 
        // that changes the width. Then apply the width we calculated. C'est la vie!
        $('img.iwansonImage.verybig').each(function (index, el) {
            let columnWidth = Math.round($(this).closest('.innerFlex').width() / oUnt);
            $(this).closest('.innerFlex').css({
                'width': oUnt * columnWidth,
            });
            let columnWidth2 = Math.round($(this).closest('.innerFlexOnlyDesktop').width() / oUnt);
            $(this).closest('.innerFlexOnlyDesktop').css({
                'width': oUnt * columnWidth2,
            });
        });
        // right Column: move image to left
        $('.contentRight img.iwansonImage.verybig').each(function (index, el) {
            $(this).css({
                'left': -oUnt * 4,
            });
            $(this).closest('.contentRight').css({
                'overflow': 'visible',
            });
            $(this).closest('.contentLeft.columnsReversed').css({
                'overflow': 'visible',
            });
        });
        $('.contentLeft img.iwansonImage.verybig').not(('.contentLeft.columnsReversed img.iwansonImage.verybig')).each(function (index, el) {
            $(this).css({
                'left': -oUnt * 2,
            });
            $(this).closest('.contentLeft').css({
                'overflow': 'visible',
            });                        
        });
        $('.contentLeft.columnsReversed img.iwansonImage.verybig').each(function (index, el) {
            $(this).css({
                'left': -oUnt * 4,
            });
            $(this).closest('.contentLeft').css({
                'overflow': 'visible',
            });                        
            
        });

        $('div.imageVerybigBodytext').css({
            'height': oUnt * 2.5,
        });
        
        
        // nimble
        $('img.iwansonImage.nimble').css({
            'height': oUnt * 8
        });
        $('.iwansonOneImage.nimble').css({
            'padding-right': (8 * oPx) + 'px'
        });
        $('.iwansonOneImage.nimble.last').css('padding-right', 0);
//        $('.nimblenr2').css('top', oUnt * 2);
//        $('.nimblenr3').css('top', oUnt / 8 * 5);
//        $('.nimblenr4').css('top', oUnt * 2);


        // admin
        $('img.iwansonImage.admin').css({
            'height': oUnt * 8,
        });
        $('.iwansonOneImage.admin').css({
            'margin-right': (4 * oPx) + 'px'
        });

        $('#contentNavigation').css('left', oUnt * 11);
        $('#contentNavigation').css('width', oUnt * 3);
        $('#contentNavigationInner').css('height', oUnt * 1.5);
        $('#contentNavigationInner').css('padding-top', oUnt / 3);
        $('#contentNavigationInner').css('padding-left', oUnt);

        $('#contentNavigationButton').css('width', oUnt * 1.5);
        $('#contentNavigationButton').css('height', oUnt * 3);
        $('#contentNavigationButton').css('bottom', oUnt * 2);
        $('#contentNavigationButton').css('left', oUnt * 11);




        // in case font size shall be adjusted:
        $('body').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        });
        $('.adminDescriptionHolder').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        });
        $('.instructorDescriptionHolder').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        }); 
        $('div.iwansonImage.instructor').not( "div.iwansonImage.instructor.guest" ).css({
            'height': oUnt*11,
        }); 
        $('.adminList').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 22) + 'px'
        });
        $('.instructorList').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 22) + 'px'
        });
        
        $("#innerWrap table").css({
            'width': 'auto',
        });
        
        
        $("#innerWrap table").each(function (index, el) {
            if ($(this).find("tr").first().children().length == 2) {
               // console.log("zweispaltige Tabelle gefunden!");
                // tabelle enthält in der ersten Zelle span mit Klasse "event"
                if ($(this).find("span").hasClass('tableLeft5')) {
                    $(this).children("tbody:first").children("tr:first").children("td:first").css({
                        'width': oUnt * 5,
                    });
                } else if ($(this).find("span").hasClass('tableLeft7')) {
                    $(this).children("tbody:first").children("tr:first").children("td:first").css("width", oUnt * 7);
                } else {
                    $(this).children("tbody:first").children("tr:first").children("td:first").css("width", oUnt * 6);
                }



            } else if ($(this).find("tr").first().children().length == 3) {
                 console.log("dreispaltige Tabelle gefunden!");
                $(this).children("tbody:first").children("tr:first").children("td:first").css("width", oUnt * 2.5);
                $(this).children("tbody:first").children("tr:first").children("td").eq(1).css("width", oUnt * 4.5);
                $(this).children("tbody:first").children("tr:first").children("td").eq(2).css("width", oUnt * 10);
            }
                $(this).children("tbody").children("tr").children("td").css({
                    'padding-right': 0,
                }); 
                $(this).children("tbody").children("tr").children("td").css({
                    'padding-bottom': oUnt /4,
                });

        });        
        $('h1').css('font-size', parseInt(oPx * 56) + 'px');
        $('h1').css('line-height', parseInt(oPx * 56) + 'px');
        $('.h2').css('font-size', parseInt(oPx * 24) + 'px');
        $('.h2').css('line-height', parseInt(oPx * 28) + 'px');
        $('h2').css('font-size', parseInt(oPx * 24) + 'px');
        $('h2').css('line-height', parseInt(oPx * 28) + 'px');
        $('.oeCal .daynumber').css('font-size', parseInt(oPx * 24) + 'px');

        $('.select-selected').css({
            'font-size': parseInt(oPx * 24) + 'px',
            'line-height': parseInt(oPx * 28) + 'px'
        });
        
        $('#footerMenuCloseButton').css({
            'top': oUnt,
            'left': oUnt * 20,
        });

        $('.event').css('font-size', parseInt(oPx * 19) + 'px');
        $('.event').css('line-height', parseInt(oPx * 25) + 'px');
        $('#menuInner').css({
            'font-size':parseInt(oPx * 19) + 'px',
            'line-height':parseInt(oPx * 50) + 'px'
        });

        $('.citation').css({
            'font-size': parseInt(oPx * 19) + 'px',
            'line-height': parseInt(oPx * 25) + 'px',
            'padding-bottom': parseInt(oUnt) + 'px'
        });


        $('a.button').css({
            'width': (oUnt * 4) - (oPx * 12),
            'height': oUnt,
            'margin-top': oUnt,
            'margin-right': oUnt

        });

// ############################################################################################################################################
// ###  MOBILE  ###############################################################################################################################
// ############################################################################################################################################        
    } else {


        let wrapWidth = $('#wrap').width();
         console.log('wrap width: ' + wrapWidth + ' - oUnt: ' + oUnt);
//
//        let logoimage = '/typo3conf/ext/iwansonsstuff/Resources/Public/Media/iwanson-logo-tablet.svg';
//        if ($('#logo img').attr('src') != logoimage) {
//            $('#logo img').attr('src', logoimage);
//        }
        // set logo size and position
        $('#logo').css({
            'width': oUnt * 6,
            'left': oUnt * 1,
            'top': oUnt,
        });
        $('#logoBackground').css({
            'width': oUnt * 9,
            'height': oUnt * 2,
        });

        $('#menuCloseButton').css({
            'top': oUnt * 0.75,
        });
        
        $('#footerMenuCloseButton').css({
            'top': oUnt/2,
            'left': oUnt * 7.5,
        });

        $('#menu table tr td').css('height', oUnt);

        $("#innerWrap table").css({
            'width': 'auto',
        });
        

        $('#menuInnerWrapper').css({
            'padding-left': oUnt,
        });

        $(' #menuInner ul li').css({
            'padding-right': oUnt / 2,
        });

        // set footer icon size and position
        //$('#footerIcon').width(oUnt * 2);
        //$('#footer').height( oUnt / 2 );
        $('#footerIcon').css({
            'left': oUnt / 2,
            'bottom': oUnt * 27/40,
            'width': oUnt
        });




        //$('#footer .footerIcon').css('border-width', oUnt / 4);
        // set margins of page itself
        $('#innerWrap').css({
            'margin-left': oUnt,
            'margin-right': oUnt,
            'padding-top': oUnt * 2
        });

        // Images. Adjust sizes according to Orientation
        $('img.iwansonImage.small').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 3);
            } else {
                $(this).css('width', oUnt * 7);
            }
        });
        $('img.iwansonImage.medium').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 3);
            } else {
                $(this).css('width', oUnt * 7);
            }
        });
        $('img.iwansonImage.big').each(function (index, el) {
            if ($(this).data('portrait-default') == 1) {
                $(this).css('width', oUnt * 3);
            } else {
                $(this).css('width', oUnt * 7);
            }
        });
//        $('img.iwansonImage.verybig').each(function (index, el) {
//            if ($(this).data('portrait-default') == 1) {
//                $(this).css('width', oUnt * 4, 5);
//            } else {
//                $(this).css('width', oUnt * 9);
//            }
//        });



        $('div.iwansonImage.verybig img').css({
            'width': wrapWidth,
        });        
        $('div.iwansonOneImage.verybig img').css({
            'width': wrapWidth,
        });          
        
        $('div.iwansonOneImage.verybig').css({
            'left': -oUnt,
        });        

        $('div.iwansonImage.instructor').css({
            'padding-right': oUnt*2,
        });        

//        $('div.iwansonImage.verybig').each(function (index, el) {
//            console.log( $(this).parent().next()   );
//            $(this).parent().next('.spacerMobileChangedContent:first').css({
//                'padding-bottom': 240,
//            });
//        });

        $('.instructorDescriptionContainer.guest').each(function (index, el) {
            if ( isEmpty( $(this) ) ) {
                $(this).css({
                    'padding': 0,
                });        
            }
        });

        $('div.iwansonImage.verybig').not( "div.iwansonImage.verybig.instructor.guest" ).css({
            'top': oUnt*3.5,
        });
        $('div.iwansonImage.verybig.instructor.guest').css({
            'height': (oUnt *4.5) + (oUnt/2) ,
        });        

        $('div.imageVerybigBodytext').css({
            'height': oUnt * 6
        });


        // nimble
        $('img.iwansonImage.nimble').css({
            'height': oUnt * 5.8
        });
        $('.iwansonOneImage.nimble').css({
            'padding-right': (4 * oPx) + 'px'
        });
        $('.iwansonOneImage.nimble.last').css('padding-right', 0);
        $('.nimblenr2').css('top', oUnt );
        $('.nimblenr3').css('top', oUnt / 8 * 2.5);
        $('.nimblenr4').css('top', oUnt );



        $('.iwansonOneImage.admin img').css({
                'height': oUnt * 5.8,
         });

        $('.iwansonOneImage.admin').not('.iwansonOneImage.admin:first').css({
                'padding-left': oUnt/10,
         });




        // in case font size shall be adjusted:
        $('body').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        });
        $('.adminDescriptionHolder').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        });
        $('.instructorDescriptionHolder').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        }); 
        $('h1').css({
            'font-size': parseInt(oPx * 40) + 'px',
            'line-height': parseInt(oPx * 40) + 'px'
        });
        $('h2').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 22) + 'px'
        });
        
        
        $('.adminList').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 22) + 'px'
        });
        $('.instructorList').css({
            'font-size': parseInt(oPx * 18) + 'px',
            'line-height': parseInt(oPx * 22) + 'px'
        });
      
        

        
        $('.citation').css({
            'font-size': parseInt(oPx * 14) + 'px',
            'line-height': parseInt(oPx * 20) + 'px'
        });


        // manually set vertical paddings betw. 
        $('.frame-space-before-extra-small').css('margin-top', 0);
        $('.frame-space-before-small').css('margin-top', 0);
        $('.frame-space-before-medium').css('margin-top', 0);
        $('.frame-space-before-large').css('margin-top', 0);
        $('.frame-space-before-extra-large').css('margin-top', 0);

        $('.frame-space-after-extra-small').css('margin-bottom', 0);
        $('.frame-space-after-small').css('margin-bottom', 0);
        $('.frame-space-after-medium').css('margin-bottom', 0);
        $('.frame-space-after-large').css('margin-bottom', 0);
        $('.frame-space-after-extra-large').css('margin-bottom', 0);

        // now (seufz) overwrite it in case second element (distance between Elements)
        $('.oneGridElement .innerFlex').find('.content').each(function (event, data) {
            $(this).find('.frame.frame-default:gt(0)').css({
                'margin-top':oUnt,
            });
        });
        
        $('.mobileDistance').css('padding-bottom', oUnt);
        $('.oneContainer.instructors ').css('padding-bottom', oUnt);

        $('.input.custom-select').css({
                'width':wrapWidth - (2*oUnt),
        });
        $('.input.custom-select select').css({
                'width':wrapWidth - (2*oUnt),
        });
        
        $('fieldset').css({
            'width':wrapWidth - (2*oUnt),
        });

        $('fieldset div.form-group').css({
            'width':wrapWidth - (2*oUnt),
        });            


        $('form input[type=text]').css({
            'width':wrapWidth - (3*oUnt),
        });   
        $('form input[type=email]').css({
            'width':wrapWidth - (3*oUnt),
        });             

//        $('.oneGridElement').css('margin-bottom', oUnt);
//        $('.instructorList').css('margin-bottom', oUnt);
    }
}

$(window).mousemove(function (event) {
    if (env == "desktop") {
        $("#mouseReplacementWrapper").css({"left": event.pageX - 160, "top": event.pageY - 160});
        //console.log($(document).scrollTop())
        if (event.pageX > oUnt * 42) {
            $("#mouseReplacement").css({
                'z-index': 100
            });
        } else if (event.pageX < oUnt * 23 && (event.pageY - $(document).scrollTop()) < oUnt * 8) {
            $("#mouseReplacement").css({
                'z-index': 100
            });
        } else if ( $("#menu").hasClass("open") ) {
            $("#mouseReplacement").css({
                'z-index': 100
            });
        } else {
            $("#mouseReplacement").css({
                'z-index': 5
            });
        }

        if (event.pageX > oUnt * 42 && event.pageX < oUnt * 45  && !$("#menu").hasClass("open") && !$("#menu").hasClass("opening") ) {
            $("#mouseReplacement .next").css({
                'display': 'block'
            });
            $("#mouseReplacement .prev").css({
                'display': 'none'
            });
        } else if (event.pageX < oUnt * 3 && !$("#menu").hasClass("open") && !$("#menu").hasClass("opening")  ) {
            $("#mouseReplacement .prev").css({
                'display': 'block'
            });
            $("#mouseReplacement .next").css({
                'display': 'none'
            });

        } else {
            $("#mouseReplacement .next").css({
                'display': 'none'
            });
            $("#mouseReplacement .prev").css({
                'display': 'none'
            });
        }
    }
});

$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results) {
        return results[1]
    }
    return -99;
}

  function isEmpty( el ){
      return !$.trim(el.html())
  }

