/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    widthFactor = [23, 17, 9, 7, 6, 17, 9, 23, 7];
    heightFactor = [9, 7, 23, 17, 14, 7, 23, 9, 17];
    marginTFactor = [1, 5];
    marginLFactor = [1, 3];
    
    lastClicked = 0;
    addToScroll = 0;

    longSide = 23;
    shortSide = 17;

    console.log("ready image tiles!");
    // now get the tile transformations done
    adjustAllTiles();
    // do the same in case window is resized
    $(window).resize(function () {
        adjustAllTiles();
    });

    if (env == "mobile" || env == "tablet") {
        // add image src to clickable object
        $('.iwansonOneImage').each(function (event, data) {
            $(this).attr('data-lc-href', $(this).find('img').attr("src") );
            if ( !$(this).attr('data-lc-href') ) {
                $(this).attr('data-lc-href', $(this).find('video source').attr("src") );
            }
        });
        
        $('.iwansonOneImage').lightcase();
        
    } else {
        // what happens on click on image in nonmobile
    
        $('div.iwansonOneImage').on("click", function (event, data) {
            //            

            
            let i = $(this).data("tile");
            console.log("#### CLICKED NUMMER " + i);
            if ($(this).data("opened")) {
                // ################## close the image ################
                console.log("close tile nr " + i + " - to " + widthFactor[ i - 1] + "/" + heightFactor[ i - 1]);
                // console.log($(this));
                $(this).data("opened", 0);

                let reducedSize = $(this).find('img').data("reducedSize");
                if (!reducedSize) {
                    reducedSize = $(this).find('video').data("reducedSize");                    
                }
                let beginMargin = $(this).find('img').data("margin");

                console.log("hide image text");
                $(this).next('.tileText').css({
                    'display': 'none',
                    opacity: 0,
                });



                adjustSpecialCases(i, true, $(this).attr('id'));
                
                let oeButton;
                // in case: stop the video
                if ( $(this).find('video').length ) {
                    $(this).find('video').get(0).pause();
                    console.log("it's a video");
                    oeButton = $(this).find('.videoPlayButtonActive');
                    console.log(oeButton);
                           oeButton.delay(700).css({
                                opacity: 1,
                            });
                }

               //console.log("reduce Size: " + reducedSize);
                if (widthFactor[ i - 1 ] > heightFactor[ i - 1 ]) {
                    $(this).find('img').velocity({
                        width: oUnt * widthFactor[ i - 1 ],
                        height: reducedSize,
                        marginTop: beginMargin
                    }, 400, "easeInOutSine", function () {
                        // complete
                    });
                } else {
                    $(this).find('img').velocity({
                        height: oUnt * heightFactor[ i - 1 ],
                        width: reducedSize,
                        marginLeft: beginMargin

                    }, 400, "easeInOutSine", function () {
                        // complete
                    });
                }

                if (i == 6) {
                    console.log("skipping animation in main, because spcial case - do all in function");
                } else {
                    // Bei Sonderfällen werden $(this)animatinen auch in adjustSpecialCases gemacht
                    // weil sonst mehrere Animationen ausgelöst werden
                      console.log("close the wrapping div in onclick");
                   $(this).velocity({
                        width: oUnt * widthFactor[ i - 1 ],
                        height: oUnt * heightFactor[ i - 1 ]
                    }, 400, "easeInOutSine", function () {
                        // complete
                    });
                }

            } else {
                // ################## open the image ################
                closeAllTilesBut($(this).attr('id'));



                $(this).data("opened", 1);
                adjustSpecialCases(i, false, $(this).attr('id'));

                // in case: start the video
                if ( $(this).find('video').length ) {
                    $(this).find('video').get(0).play();
                    $(this).find('.videoPlayButtonActive').css({
                        opacity: 0,
                    });                    
                }
                
                
                if ( lastClicked >  i) {
                    console.log ("backwards reset addToScroll")
                    addToScroll = 0;
                }
                 console.log( '### ADD TO SROLL on this click: ' + addToScroll ); 

                $([document.documentElement, document.body]).animate({
                    scrollTop: $(this).offset().top - (oUnt * 8) - (addToScroll * oUnt) 
                }, 400);

            
                // querformat
                if ($(this).width() > $(this).height()) {
                    if (i == 6) {
                        console.log("skipping animation in main, because spcial case - do all in function");
                    } else {
                        $(this).velocity({
                            width: oUnt * longSide,
                            height: oUnt * shortSide
                        }, 400, "easeInOutSine", function () {
                            // complete
                            console.log("show image text");
                            $(this).next('.tileText').css({
                                'display': 'block',
                                opacity: 1,
                            });

                        });
                    }
                    $(this).find('img').velocity({
                        marginTop: 0,
                        marginLeft: 0,
                        width: oUnt * longSide,
                        height: oUnt * shortSide

                    }, 400, "easeInOutSine", function () {
                        // complete
                    });

                } else {
                    // hochformat                
                    if (i == 6) {
                        console.log("skipping animation in main, because spcial case - do all in function");
                    } else {
                        
                        // Bei Sonderfällen werden $(this)animatinen auch in adjustSpecialCases gemacht
                        // weil sonst mehrere Animationen ausgelöst werden
                        $(this).velocity({
                            width: oUnt * shortSide,
                            height: oUnt * longSide
                        }, 400, "easeInOutSine", function () {
                            // complete
                            console.log("show image text");
                            $(this).next('.tileText').css({
                                'display': 'block',
                                opacity: 1,
                            });

                        });
                    }
                    $(this).find('img').velocity({
                        marginTop: 0,
                        marginLeft: 0,
                        width: oUnt * shortSide,
                        height: oUnt * longSide

                    }, 400, "easeInOutSine", function () {
                        // complete
                    });

                }
                // now prepare addToScroll for the next click (in case
                // the no open image takes additional height)
                switch (i) {
                    case 1:
                        addToScroll = 4;
                        break;
                    case 5:
                        addToScroll = 9;
                        break;
                    case 6:
                        addToScroll = 6;
                        break;
                    case 8:
                        addToScroll = 6;
                        break;
                    default:
                        addToScroll = 0;
                    break;
                }
                console.log( '### ADD TO SROLL for next click: ' + addToScroll ); 
                lastClicked = i;
            }

        });
    // END: what happens on click on image in nonmobile
    }


});

function adjustSpecialCases(i, closing, clickedObjectID) {
    
    // ##### tile 3 #####
    if (i == 3) {
       console.log("adjust for " + i + " - closing: " + closing);
       console.log("object ID: " + clickedObjectID);

       let adjustMe = 2.5;
        if (closing) {
            let targetValue = parseInt($('#' + clickedObjectID).parent().prev().css("margin-left").replace("px", "")) + (oUnt * adjustMe);
            console.log("targetValue: "+targetValue);
            $('#' + clickedObjectID).parent().prev().velocity({
                marginLeft: targetValue,
            }, 400, "easeInOutSine", function () {
                // complete
            });
        } else {
            let targetValue = parseInt($('#' + clickedObjectID).parent().prev().css("margin-left").replace("px", "")) - (oUnt * adjustMe);
            console.log("targetValue: "+targetValue);
            $('#' + clickedObjectID).parent().prev().velocity({
                marginLeft: targetValue,
            }, 400, "easeInOutSine", function () {
                // complete
            });
        }
    }
    // ##### tile 4 #####
    else if (i == 4) {
       console.log("adjust for " + i + " - closing: " + closing);
       console.log("object ID: " + clickedObjectID);
        let adjustMe = 9.5;
        if (closing) {
            let targetValue = parseInt($('#' + clickedObjectID).parent().css("margin-left").replace("px", "")) + (oUnt * adjustMe);
            $('#'+clickedObjectID).parent().velocity({
                marginLeft: targetValue,
            }, 400, "easeInOutSine", function () {
                // complete
            });
        } else {
            let targetValue = parseInt($('#' + clickedObjectID).parent().css("margin-left").replace("px", "")) - (oUnt * adjustMe);
            $('#'+clickedObjectID).parent().velocity({
                marginLeft: targetValue,
            }, 400, "easeInOutSine", function () {
                // complete
            });
        }
    }

    // ##### tile 5 #####
    else if (i == 5) {
       console.log("adjust for " + i + " - closing: " + closing);
       console.log("object ID: " + clickedObjectID);
        let adjustMe = 12.5;

        if (closing) {
            console.log("adjust spcial case 5 CLOSING");
            let targetValue = 0;
            
            $('#'+clickedObjectID).parent().nextAll("div.clear.tile5").first().velocity({
                height: targetValue,
            }, 400, "easeInOutSine", function () {
                // complete
            });
        } else {
             console.log("adjust spcial case 5 OPENING");
           let targetValue = oUnt * adjustMe;
            $('#'+clickedObjectID).parent().nextAll("div.clear.tile5").first().velocity({
                height: targetValue,
            }, 400, "easeInOutSine", function () {
                // complete
            });

        }
    }

    // ##### tile 6 #####
    else if (i == 6) {
       console.log("adjust for " + i + " - closing: " + closing);
       console.log("object ID: " + clickedObjectID);
        console.log(clickedObjectID);
        let adjustMe = 4;
        if (closing) {
             console.log("adjust spcial case 6 CLOSING");
            let targetValue = 0;
            let targetValue2 = parseInt($('#' + clickedObjectID).css("margin-left").replace("px", "")) + (oUnt * adjustMe);
            let reducedSize = $('#' + clickedObjectID).find('img').data("reducedSize");
            if (!reducedSize) {
                reducedSize = $('#' + clickedObjectID).find('video').data("reducedSize");
            }
            console.log("reducedSize: " + reducedSize);


//            $('div.clear.tile5').velocity({
//                height: targetValue,
//            }, 400, "easeInOutSine", function () {
//                // complete
//
//            });

            console.log("hide image text");
            $('#' + clickedObjectID).next('.tileText').css({
                'display': 'none',
                opacity: 0,
            });
            $('#' + clickedObjectID).velocity({
                width: oUnt * widthFactor[ i - 1 ],
                height: reducedSize,
                marginLeft: targetValue2,
                marginTop: 0
            }, 400, "easeInOutSine", function () {
                // complete

            });
        } else {
             console.log("adjust spcial case 6 OPENING");
            
            let targetValue = oUnt * adjustMe;
            let targetValue2 = parseInt($('#' + clickedObjectID).css("margin-left").replace("px", "")) - (oUnt * adjustMe);
//            $('div.clear.tile5').velocity({
//                height: targetValue,
//            }, 400, "easeInOutSine", function () {
//                // complete
//            });
//            console.log(oUnt * widthFactor[ i - 1 ]);
            $('#' + clickedObjectID).velocity({
                width: oUnt * longSide,
                height: oUnt * shortSide,
                marginLeft: targetValue2,
                marginTop: oUnt*4
                
            }, 400, "easeInOutSine", function () {
                // complete
                console.log("show image text");
                $('#' + clickedObjectID).next('.tileText').css({
                    'display': 'block',
                    opacity: 1,
                });

            });
        }
    }

    // ##### tile 7 #####
    else if (i == 7) {
       console.log("adjust for " + i + " - closing: " + closing);
       console.log("object ID: " + clickedObjectID);
        let adjustMe = 9.5;
        if (closing) {
            console.log("tile 7 adjust CLOSING");
            $('#' + clickedObjectID).parent().next(".iwansonImage").velocity({
                width: oUnt * 23,
                
            }, 400, "easeInOutSine", function () {
            $('#' + clickedObjectID).parent().next(".iwansonImage").css("overflow",'visible');         
            });
//            $('#' + clickedObjectID).parent().next(".iwansonImage").find(".iwansonOneImage picture img").first().velocity({
//                width: oUnt * 23,
//                'margin-top': -oUnt * 4,
//            }, 400, "easeInOutSine", function () {
//                // complete
//            });
        } else {
            console.log("tile 7 adjust OPENING");
            $('#' + clickedObjectID).parent().next(".iwansonImage").css("overflow",'hidden');         
            $('#' + clickedObjectID).parent().next(".iwansonImage").velocity({
                width: oUnt * 17,
            }, 400, "easeInOutSine", function () {
                // complete
            });
            
//            $('#' + clickedObjectID).parent().next(".iwansonImage").find(".iwansonOneImage picture img").first().velocity({
//                width: oUnt * 17,
//                'margin-top': 0,
//            }, 400, "easeInOutSine", function () {
//                // complete
//            });

        }
    }

    // ##### tile 9 #####
    else if (i == 9) {
       console.log("adjust for " + i + " - closing: " + closing);
       console.log("object ID: " + clickedObjectID);
        let adjustMe = 9.5;

        if (closing) {
            $('#' + clickedObjectID).parent().velocity({
                height: oUnt * heightFactor[ i - 1 ],
                'margin-left': oUnt * 10
            }, 400, "easeInOutSine", function () {
                // complete
            });
        } else {
            $('#' + clickedObjectID).parent().velocity({
                height: oUnt * longSide,
                'margin-left': oUnt * 5
            }, 400, "easeInOutSine", function () {
                // complete
            });
        }
    }

}

function closeAllTilesBut(clickedTile) {
    $('.iwansonOneImage').each(function (index, el) {
        if ($(this).attr("id") != clickedTile) {
            if ($(this).data("opened")) {
                console.log('closing tile ' + $(this).data("tile"));

                $(this).next('.tileText').css({
                    'display': 'none',
                    opacity: 0,
                });

                let oeButton;
                // in case: stop the video
                if ( $(this).find('video').length ) {
                    $(this).find('video').get(0).pause();
                    console.log("it's a video");
                    oeButton = $(this).find('.videoPlayButtonActive');
                    console.log(oeButton);
                           oeButton.delay(700).css({
                                opacity: 1,
                            });
                }

//TODO: redundanz
                let i = $(this).data("tile");

                console.log("close tile nr " + i + " - to " + widthFactor[ i - 1] + "/" + heightFactor[ i - 1]);
                // console.log($(this));
                $(this).data("opened", 0);

                let reducedSize = $(this).find('img').data("reducedSize");
                if (!reducedSize) {
                    reducedSize = $('#' + clickedTile).find('video').data("reducedSize");
                }
                
                let beginMargin = $(this).find('img').data("margin");

                adjustSpecialCases(i, true, $(this).attr('id'));
                //console.log("reduce Size: " + reducedSize);
                if (widthFactor[ i - 1 ] > heightFactor[ i - 1 ]) {
                    $(this).find('img').velocity({
                        width: oUnt * widthFactor[ i - 1 ],
                        height: reducedSize,
                        marginTop: beginMargin
                    }, 400, "easeInOutSine", function () {
                        // complete
                    });
                } else {
                    $(this).find('img').velocity({
                        height: oUnt * heightFactor[ i - 1 ],
                        width: reducedSize,
                        marginLeft: beginMargin

                    }, 400, "easeInOutSine", function () {
                        // complete
                    });
                }

                if (i == 6) {
                    console.log("skipping animation in main, because spcial case - do all in function");
                } else {
                    // Bei Sonderfällen werden $(this)animatinen auch in adjustSpecialCases gemacht
                    // weil sonst mehrere Animationen ausgelöst werden
                     console.log("close the wrapping div in adjustspecialcases");
                   $(this).velocity({
                        width: oUnt * widthFactor[ i - 1 ],
                        height: oUnt * heightFactor[ i - 1 ]
                    }, 400, "easeInOutSine", function () {
                        // complete
                    });
                }


            }
        }
    });
}

function adjustAllTiles() {
    console.log("adjust All Tiles... ");

    if (env == "desktop") {
// ###############################################################################################################################
// ###  DESKTOP  #################################################################################################################
// ###############################################################################################################################
        console.log("... for desktop");
        // columns in Content are calculated by flex
        // get size of one Unity ( one quad in grid )
        // using columns calculated by flex as a basis
        console.log('oPx: ' + oPx);


        $('.iwansonimagetiles .tilesBodytext').css('margin-left', oUnt * 24.5);
        $('.iwansonimagetiles .tilesBodytext').css('margin-top', -(oUnt * 4));
        $('.iwansonimagetiles .tilesBodytext').css('height', oUnt * 6);

        $('div.iwansonImage.tile1').css({
            'margin-left': oUnt * 17.5,
            'margin-bottom': oUnt * 1
        });
        
        $('div.iwansonOneImage.tile1').css({
            'width': oUnt * 23,
            'height': oUnt * 9,
        });

        $('div.iwansonImage.tile2').css({
            'margin-left': oUnt * 6.5,
        });
        
        $('div.iwansonOneImage.tile2').css({
            'width': oUnt * 17,
            'height': oUnt * 7,
        });

        $('div.iwansonImage.tile3').css({
            'height': oUnt * 23,
            'width': oUnt * 9,
            'margin-left': oUnt * 1,
            'margin-top': oUnt * 3
        });

        $('div.iwansonOneImage.tile3').css({
            'height': oUnt * 23,
            'width': oUnt * 9,
        });

        $('div.iwansonImage.tile4').css({
            'height': oUnt * 17,
            'margin-top': -(oUnt * 3.5),
            'margin-left': oUnt * 8.5,
       });

        $('div.iwansonOneImage.tile4').css({
            'height': oUnt * 17,
            'width': oUnt * 7,
        });

        $('div.iwansonImage.tile5').css({
            'height': oUnt * 14,
            'width': oUnt * 6,            
            'margin-left': oUnt * 1,
            'margin-top': oUnt * 7.5,
        });
        $('div.iwansonOneImage.tile5').css({
            'height': oUnt * 14,
            'width': oUnt * 6,            
        });
        
        $('div.iwansonImage.tile6').css({
           'width': oUnt * 17,
            'margin-top': -(oUnt * 2.5),
            'margin-left': oUnt * 23.5,
        });
        $('div.iwansonOneImage.tile6').css({
            'width': oUnt * 17,
            'height': oUnt * 7,
        });

        $('div.iwansonImage.tile7').css({
            'height': oUnt * 23,
            'margin-left': oUnt * 5.5,
            'margin-top': -(oUnt * 2.5),
        });

                
        $('div.iwansonOneImage.tile7').css({
            'width': oUnt * 9,
            'height': oUnt * 23,
        });


        $('div.iwansonImage.tile8').css({
            'width': oUnt * 23,
            'margin-left': oUnt,
            'margin-top': oUnt,
        });
                
        $('div.iwansonOneImage.tile8').css({
            'height': oUnt * 9,
            'width': oUnt * 23,
        });
        

        $('div.iwansonImage.tile9').css({
            'height': oUnt * 17,
            'margin-left': oUnt * 10,
            'margin-top': oUnt,
            'margin-bottom': oUnt,
        });

        $('div.iwansonOneImage.tile9').css({
            'height': oUnt * 17,
            'width': oUnt * 7,
        });
        

        $('.iwansonImage.tile3 .tileText').css({
            'bottom':-(oUnt/2*3),
        });
        $('.iwansonImage.tile4 .tileText').css({
            'bottom':-(oUnt*7),
        });
        
        $('.iwansonImage.tile5 .tileText').css({
            'bottom':-(oUnt*5.5),
            'right':oUnt*6,
            'width':oUnt*10,
        });
        $('.iwansonImage.tile6 .tileText').css({
            'bottom':(oUnt*10),
            'width':(oUnt*9),
            'left':-(oUnt*13),
        });
        $('.iwansonImage.tile8 .tileText').css({
            'bottom':-(oUnt),
        });     
        // adjust the images inside the image container
        for (i = 1; i < 10; i++) {
            if (widthFactor[ i - 1 ] > heightFactor[ i - 1 ]) {
                let adjustedHeight = widthFactor[ i - 1 ] / longSide * shortSide;
                $('.iwansonOneImage.tile' + i + ' img').data("reducedSize", oUnt * adjustedHeight);
                $('.iwansonOneImage.tile' + i + ' video').data("reducedSize", oUnt * adjustedHeight);
                let margin = (heightFactor[ i - 1 ] - adjustedHeight) / 2;
                $('.iwansonOneImage.tile' + i + ' img').data("margin", oUnt * margin);
                $('.iwansonOneImage.tile' + i + ' video').data("margin", oUnt * margin);
                $('.iwansonOneImage.tile' + i + ' img').css({
                    "width": oUnt * widthFactor[ i - 1 ],
                    "margin-top": oUnt * margin,
                });

            } else {
                let adjustedWidth = heightFactor[ i - 1 ] / longSide * shortSide;
                $('.iwansonOneImage.tile' + i + ' img').data("reducedSize", oUnt * adjustedWidth);
                $('.iwansonOneImage.tile' + i + ' video').data("reducedSize", oUnt * adjustedWidth);
                let margin = (widthFactor[ i - 1 ] - adjustedWidth) / 2;
                $('.iwansonOneImage.tile' + i + ' img').data("margin", oUnt * margin);
                $('.iwansonOneImage.tile' + i + ' img').css({
                    "height": oUnt * heightFactor[ i - 1 ],
                    "margin-left": oUnt * margin,
                });

            }

        }


// ###############################################################################################################################
// ###  TABLET  ##################################################################################################################
// ###############################################################################################################################
    } else if (env == "tablet") {
        console.log("... for tablet");

        $('.iwansonimagetiles .tilesBodytext').css('margin-left', oUnt * 16);
        $('.iwansonimagetiles .tilesBodytext').css('margin-top', -(oUnt * 2));
        $('.iwansonimagetiles .tilesBodytext').css('height', oUnt * 3);

        $('div.iwansonImage.tile1').css({
            'margin-top': -(oUnt /2),
            'margin-left': oUnt * 7,
            'margin-bottom': oUnt /2
        });
        $('div.iwansonOneImage.tile1').css({
            'width': oUnt * 13,
            'height': oUnt * 5,
        });
//        $('div.iwansonOneImage.tile1 img').css({
//        });
    

        $('div.iwansonImage.tile2').css({
            'margin-left': oUnt*2,
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile2').css({
            'width': oUnt * 9,
            'height': oUnt * 4,
        });        
        
        $('div.iwansonImage.tile3').css({
            'width': oUnt * 5,
            'height': oUnt * 13,
            'margin-left': oUnt /2,
            'margin-top': 0
        });
        $('div.iwansonOneImage.tile3').css({
            'width': oUnt * 5,
            'height': oUnt * 13,
        });     
        $('div.iwansonImage.tile4').css({
            'height': oUnt * 9,
             'width': oUnt * 4,
           'margin-top': -(oUnt * 3.5),
            'margin-left': oUnt * 3.5,
        });      
        $('div.iwansonOneImage.tile4').css({
            'height': oUnt * 9,
             'width': oUnt * 4,
        });  
        
       $('div.iwansonImage.tile5').css({
            'height': oUnt * 7,
             'width': oUnt * 3,            
            'margin-left': oUnt /2,
            'margin-top': -(oUnt * 1.5),
            
        });
        $('div.iwansonOneImage.tile5').css({
            'height': oUnt * 7,
             'width': oUnt * 3,
        });  
        
        $('div.iwansonImage.tile6').css({
            'height': oUnt * 4,
             'width': oUnt * 9,
            
            'margin-top': -(oUnt * 5),
            'margin-left': oUnt * 11.5,
        });        
        $('div.iwansonOneImage.tile6').css({
            'height': oUnt * 4,
             'width': oUnt * 9,
        });  
        $('div.iwansonImage.tile7').css({
            'height': oUnt * 13,
              'width': oUnt * 5,
           'margin-left': oUnt * 1.5,
            'margin-top': oUnt*3,
        })
        $('div.iwansonOneImage.tile7').css({
            'height': oUnt * 13,
              'width': oUnt * 5,
        });  
        $('div.iwansonImage.tile8').css({
            'height': oUnt * 4.5,
              'width': oUnt * 9,
            'margin-left': oUnt/2,
            'margin-top': oUnt/2,
        });
        $('div.iwansonOneImage.tile8').css({
            'height': oUnt * 4.5,
              'width': oUnt * 9,
        });         
        $('div.iwansonImage.tile9').css({
            'height': oUnt * 9,
             'width': oUnt * 4,
            'margin-left': oUnt /2,
            'margin-right': oUnt *4,
            'margin-top': oUnt,
            'margin-bottom': oUnt,
        });        
        $('div.iwansonOneImage.tile9').css({
            'height': oUnt * 9,
             'width': oUnt * 4,
        });  
        
// ###############################################################################################################################
// ###  MOBILE  ##################################################################################################################
// ###############################################################################################################################        
    } else {
        console.log("... for mobile");
        $('.iwansonimagetiles .tilesBodytext').css('margin-left', 0);
        $('.iwansonimagetiles .tilesBodytext').css('margin-top', 0);
        $('.iwansonimagetiles .tilesBodytext').css('height', 0);

        $('div.iwansonImage.tile1').css({
            'margin-left': oUnt * 2,
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile1').css({
            'width': oUnt * 6,
            'height': oUnt * 3,
        });
        $('div.iwansonOneImage.tile1 img').css({
            'margin-left': -(oUnt * 2),
        });

        $('div.iwansonImage.tile2').css({
            'margin-left': 0,
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile2').css({
            'width': oUnt * 6,
            'height': oUnt * 3,
        });
        $('div.iwansonOneImage.tile2 img').css({
        });

        $('div.iwansonImage.tile3').css({
            'margin-left': oUnt / 1.5,
            'margin-top': oUnt,
        });
        $('div.iwansonOneImage.tile3').css({
            'width': oUnt * 3,
            'height': oUnt * 6,
            'float': 'left',
            'margin-right': oUnt / 4
        });

        $('div.iwansonImage.tile4').css({
            'margin-top': -(oUnt),
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile4').css({
            'width': oUnt * 3,
            'height': oUnt * 6,
        });

        $('div.iwansonImage.tile5').css({
            'margin-top': -1,
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile5').css({
            'width': oUnt * 3,
            'height': oUnt * 6,
        });

        $('div.iwansonImage.tile6').css({
            'margin-left': 0,
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile6').css({
            'width': oUnt * 6,
            'height': oUnt * 3,
        });


        $('div.iwansonImage.tile7').css({
            'margin-left': oUnt / 1.5,
        });
        $('div.iwansonOneImage.tile7').css({
            'width': oUnt * 3,
            'height': oUnt * 6,
            'float': 'left',
            'margin-bottom': oUnt / 4
        });

        $('div.iwansonImage.tile8').css({
            'margin-left': 0,
            'margin-bottom': oUnt / 4
        });
        $('div.iwansonOneImage.tile8').css({
            'width': oUnt * 6,
            'height': oUnt * 3,
        });


        $('div.iwansonImage.tile9').css({
            'margin-left': oUnt / 1.5,
        });
        $('div.iwansonOneImage.tile9').css({
            'width': oUnt * 3,
            'height': oUnt * 6,
            'float': 'left',
            'margin-bottom': oUnt / 4
        });

    }
}